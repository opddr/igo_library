# 1. Generalized linear model

일반선형 모델은 Maximum likelihood estimation을 하는데 좋은 환경을 제공한다.

1. score function과 information function을 구하기 위한 canonical form 보장
2. natural parameter를 이용한 activation 함수 선형화에 필요한 canonical link function 제공

일반선형모델은 아래와 같이 정의된다.

1. observation $Y_i$들은 서로 독립이다.
2. observation $Y_i$의 분포는 canonical form이다.
3. $g(\mu_i)=\eta_i$를 만족하는 식이 존재하며 역함수가 존재한다. g를 canonical link function이라고 하며, linear regression의 activation 함수의 역함수 이다. canonical form의 natural parameter에 estimation인 $\mu$를 넣으면 linear regression이 나오기 때문에, natural parameter를 canonical link function으로 봐도 된다. 

# 2. Exponential family(지수족)

확률분포는 사실 exponential family의 부분집합이다.

지수족에 속한 확률 분포들은 아래와 같다.


  * normal
  * exponential
  * gamma
  * chi-squared
  * beta
  * Dirichlet
  * Bernoulli
  * categorical
  * Poisson
  * Wishart
  * inverse Wishart
  * geometric

  지수족은 아래와 같이 다양한 방법 표현된다.

  $f_X(x|\theta)=h(x)e^{(\eta(\theta)\cdot T(x)-A(\theta))}\\f_X(x|\theta)=h(x)g(\theta)e^{(\eta(\theta)\cdot T(x))}\\f_X(x|\theta)=e^{(\eta(\theta)\cdot T(x)-A(\theta)+B(x))}\\f_Y(y;\theta)=e^{(a(y)b(\theta)+c(\theta)+d(y))}$


  # 3. Canonical form

  지수족에 속한 분포들 중

  $f_Y(y;\theta)=e^{(a(y)b(\theta)+c(\theta)+d(y))}$


  로 표현 했을 때,  a(y)=y를 만족하는 분포들은 canonical form이라고 하며, 정규분포, 이항분포, 포아송 분포들이 canonical form형태로 표현 가능하다.

  canonical form으로 표현된 likelihood 함수에 logarithm을 취하고 한 번 미분하거나 2번 미분 하게되면 특정 component 들이 확률 분포의 평균이나 분산임을 볼수 있다.
  이를 이용하여 canonical form의 항들로 확률분포의 평균과 분산을 표현할 수 있게 된다.

  또한 canonical form에서 maximum likelihood estimation를 하는데 핵심 요소인 score 함수와 information 함수를 
  유도할 수 있다. score 함수는 log likelhood 함수의 1계도함수이고, information 함수는 score함수의 분산이다.

  ## 3.1. 확률분포의 평균과 분산

  확률밀도함수 $f(y;\theta)$의 면적의 합은 1임을 알고 있을 것이다. 따라서 아래의 수식이 성립한다.

  $\int f(y;\theta)dy=1$

  $\frac{d}{d\theta}\int{f(y;\theta)dy}=0$

  $\int{\frac{d}{d\theta}f(y;\theta)dy}=0$

  $\int{\frac{d^2}{d\theta^2}f(y;\theta)dy}=0$

  위의 수식에서 우리는 확률분포의 평균과 분산을 유도할 수 있다.


  $\frac{d}{d\theta}f(y;\theta)=[a(y)b'(\theta)+c'(\theta)]f(y;\theta)$


  $\int\frac{d}{d\theta}f(y;\theta)=\int[a(y)b'(\theta)+c'(\theta)]f(y;\theta)dy$


  $0=b'(\theta)\int{a(y)f(y;\theta)dy} +c'(\theta)\cdot\int{f(y;\theta)dy}$


  $0=b'(\theta)E[a(Y)]+c'(\theta)\cdot 1$

  

  $E[a(Y)]=-\frac{c'(\theta)}{b'(\theta)}$

  

  이번엔 2계도함수에서 분산을 유도해 보자.


  $\frac{d^2}{d\theta^2}f(y;\theta)=[a(y)b''(\theta)+c''(\theta)]f(y;\theta)+[a(y)b'(\theta)+c'(\theta)]^2f(y;\theta)$

  $\int\frac{d^2}{d\theta^2}f(y;\theta)dy=\int[a(y)b''(\theta)+c''(\theta)]f(y;\theta)dy+\int[a(y)b'(\theta)+c'(\theta)]^2f(y;\theta)dy$

  $0=b''(\theta)E[a(Y)]+c''(\theta)+[b'(\theta)]^2\displaystyle\int[a(y)+\frac{c'(\theta)}{b'(\theta)}]^2f(y;\theta)dy$

  $0=-b''(\theta)\frac{c'(\theta)}{b'(\theta)}+c''(\theta)+[b'(\theta)]^2\displaystyle\int{\{a(y)-E[a(Y)]\}^2f(y;\theta)dy}$

  $0=-b''(\theta)\frac{c'(\theta)}{b'(\theta)}+c''(\theta)+[b'(\theta)]^2Var[a(Y)]$

  $Var[a(Y)]=\frac{b''(\theta)c'(\theta)-c''(\theta)b'(\theta)}{[b'(\theta)]^3}$

  


  ## 3.2. Score function and information function

  GLM에서 유용한 두 함수가 있다. 하나는 likelihood function의 1계도함수인 score function과 score function의 분산인 information 함수 이다.

  $f(\theta;y)=\displaystyle\prod_{i=1}^{n}{e^{\{a(y_i)b(\theta)+c(\theta)+d(y_i)\}}}$

  $\log f(\theta;y)=\displaystyle\sum_{i=1}^{n}{\{a(y_i)b(\theta)+c(\theta)+d(y_i)\}}\\b(\theta)\displaystyle\sum_{i=1}^{n}{a(y_i)}+nc(\theta)+\displaystyle\sum_{i=1}^{n}{d(y_i)}$

  따라서 score function $U$는 아래와 같다.

  $U\equiv \frac{\partial}{\partial\theta}\log f(\theta;y)=b'(\theta)\cdot\displaystyle\sum_{i=1}^{n}{a(y_i)}+nc'(\theta)$


  U의 분산인 information function인 $\mathcal J$는 아래와 같다.

  $\mathcal J=Var[b'(\theta)\displaystyle\sum_{i=1}^{n}{a(Y_i)}+n'c(\theta)]\\=[b'\theta]^2\cdot n(\frac{b''(\theta)c'(\theta)-c''(\theta)b'(\theta)}{[b'(\theta)]^3})\\=n(\frac{b''(\theta)c'(\theta)}{b'(\theta)}-c''(\theta))\\=E[U^2]\\=-E[U']$

  


  # 4. maximum likelihood estimation

  maximum likelihood estimation은 regression의 parametric model에서 likelihood function이 최대값을 만족하는 파라미터 $\hat\beta$을 찾는 과정이다.

  최대값은 미분 계수가 0이고 2계도함수가 negative definite을 만족시키는 $\beta_i$에 존재하지만, 유사도 함수가 non-convex라면 local-maxima에 빠질 수 있으니 stochastic 메커니즘이 도입되야한다.

  결론적으로 유사도 함수의 1계도함수가 0되는 지점을 찾아야 하는데, 1계도함수=0인 방정식의 해를 구할 수 없는 non-closed form이라면 newton-raphson이나 quasi-newton을 사용해서 gradient descent를 통헤 $\hat\beta$의 근사값을 찾아야 한다.

  ## 4.1. closed form 

  우선 information function $U$=0 을 만족하는 closed form에서 MLE를 만족하는 $\hat\beta$를 구해보자.

  ### 4.1.1. Score function

  $f_{Y}(y_i;\theta_i)=e^{\{y_ib(\theta_i)+c(\theta_i)+d(y_i)\}}$

  $\log \mathcal L(\theta;y)=\displaystyle\sum_{i=1}^{n}{y_ib(\theta_i)}+\displaystyle\sum_{i=1}^{n}{c(\theta_i)}+\displaystyle\sum_{i=1}^{n}{d(y_i)}=\displaystyle\sum_{i=1}^{n}{l_i}$

  $l_i$는 estimation인 $\theta$, linear regression인 $\eta=\displaystyle\sum_{k=1}^{K}{x_k\beta_k}$

  parameter인 $\beta_k$가 차례로 합성된 함수이므로

  

  $\nabla\log{L(\theta;y)}$는 $U_j=\displaystyle\sum_{i=1}^{n}{\frac{\partial l_i}{\partial\theta_i}\frac{\partial\theta_i}{\partial\eta_i}\frac{\partial\eta_i}{\partial\beta_j}}$로 이루어진 Jacobian matrix 이다.

  

  이제 chain rule을 풀어보자.

  * $\frac{\partial l_i}{\partial\theta_i}$

  $\frac{\partial l_i}{\partial\theta_i}=y_ib'(\theta_i)+c'(\theta_i)=b'(\theta)\{y_i+\frac{c'(\theta_i)}{b'(\theta_i)}\}$

  그런데 canonical form 에서 $a(y)=y$, $E[a(y)]=E[y]=-\frac{c'(\theta)}{b'(\theta)}=\mu_i$가 성립함을 알 수 있다. 따라서 아래와 같은 결론을 내릴 수 있다.

  $\frac{\partial l_i}{\partial\theta_i}=\frac{\partial b(\theta_i)}{\partial\theta_i}(y_i-\mu_i)$

  


  * $\frac{\partial\theta_i}{\partial\eta_i}$

  estimation은 활성화 함수의 종류에 따라 다르므로, 활성화 함수가 결정되야 수식 도출이 가능하다.

  $\frac{\partial\theta_i}{\partial\eta_i}=\frac{\partial\theta_i}{\partial\eta_i}$

 


  * $\frac{\partial\eta}{\partial\beta_j}$

  $\eta=\displaystyle\sum_{k=1}^{K}{x_k\beta_k}$

  $\frac{\partial\eta}{\partial\beta_j}=x_j$

  


  * conclusion

  $U_j=\displaystyle\sum_{i=1}^{n}{\frac{\partial b(\theta_i)}{\partial\theta_i}(y_i-\mu_i)\cdot x_j\cdot\frac{\partial\theta_i}{\partial\eta_i}}$

  

  natural parameter의 미분과 activation 함수의 미분은 아래의 각각의 경우에서 계산해보자.



  #### 4.1.1.1. Normal distribution case

  정규분포의 activation 함수는 identity함수이므로, $\theta=\eta$이며,  canonical form으로 표현하면 아래와 같이된다.

  $f_{Y}(y_i;\theta_i)=e^{\{y_ib(\theta_i)+c(\theta_i)+d(y_i)\}}=e^{(y\frac{\theta}{\sigma^2}-\frac{\theta^2}{2\sigma^2}-\frac{y^2}{2\sigma^2}-\frac{1}{2}log(2\pi\sigma^2))}$

  

  따라서 $\frac{\partial\theta_i}{\partial\eta_i}$는 1이되고 $\frac{\partial b(\theta_i)}{\partial\theta_i}$는 $\frac 1{\sigma^2}$되기 때문에 score function은 아래와 같이 된다.

  

   $U_j=\frac {1}{\sigma^2}\displaystyle\sum_{i=1}^{n}{(x_jy_i-x_j\mu_i)}$

  

  #### 4.1.1.2. Bernoulli distribution case

  이항분포의 activation 함수는 logistic sigmoid 함수 $\frac{e^{\eta}}{e^{\eta}+1}$이고 canonical form으로 표현하면 

  $f_{Y}(y_i;\theta_i)=e^{\{y_ib(\theta_i)+c(\theta_i)+d(y_i)\}}=e^{\{y\log{(\frac{\theta}{1-\theta})+\log(1-\theta)}\}}$

  



  * $\frac{\partial b(\theta_i)}{\partial\theta_i}$

  $\frac{\partial b(\theta_i)}{\partial\theta_i}=\frac{\partial }{\partial\theta_i}\log(\frac{\theta}{1-\theta})=\frac{\partial }{\partial\theta_i}(\log\theta-\log{(1-\theta)})\\=\frac{1}{\theta}-\frac{1}{1-\theta}\cdot-1$

$=\frac{1}{\theta-\theta^2}$



* $\frac{\partial\theta_i}{\partial\eta_i}$

https://towardsdatascience.com/derivative-of-the-sigmoid-function-536880cf918e 참고

$\frac{\partial\theta_i}{\partial\eta_i}=\frac{e^{\eta_i}}{e^{\eta_i}+1}(1-\frac{e^{\eta_i}}{e^{\eta_i}+1})=\theta-\theta^2$



* conclusion

  $U_j=\displaystyle\sum_{i=1}^{n}{\frac{1}{\theta-\theta^2}(y_i-\mu_i)\cdot x_j\cdot(\theta-\theta^2)}\\=\displaystyle\sum_{i=1}^{n}{(x_jy_i-x_j\mu_i)}$




  ## 4.2. non closed form 

  ### 4.2.1. Newton-raphson method(newton method)

  1. 초기 파라미터 $\beta^{(0)}$를 설정
  2. $\beta^{(m)}=\beta^{(m-1)}-[\mathbf H^{(m-1)}]^{-1}U^{(m-1)}$
  3. k번 반복(k는 사용자 설정)하거나 수렴할때까지 반복



2번의 식에서 $\mathbf H$는 $\beta$에 대한 $`\log\mathcal L`$의 2계도함수로 이루어진 행렬 이다.

MLE에서는 $\mathbf H$대신 $\mathbf H$의 평균값을 사용한다(근사차원으로..).



$\mathbf H$는 score function을 한번 더 미분한 것이기 때문에, $`\mathbf H=U'`$라고 할 수 있으며

 3.2. 절에서 유도한 information 함수(  $\mathcal J=E[U^2]=-E[U']$)를 유도한것을 이용하면 

아래와 같이 2번수식을 변경할 수 있다.

$\beta^{(m)}=\beta^{(m-1)}+[\mathcal J^{(m-1)}]^{-1}U^{(m-1)}$



  그런데 유사도 함수가 non-convex인 경우 $\beta^{(i)}$가 수렴해도 MLE이가 아닌 local maxima 일 수 있다. 따라서 stochastic 메커니즘이 도입되야 한다.

* $\mathcal J$ 유도하기

$\mathcal J=-E[U']=E[U*U]$

$-E[U'_{jk}]=E[U_jU_k]=E[x_jx_k(\displaystyle\sum^{n-1}_{i=0}{(y_i-\mu_i)})^2]$
$=E[x_jx_k(\displaystyle\sum_{i=0}^{n-1}{(y_n-\mu_i)^2})+\displaystyle\sum_{i=0}^{m-1}{\displaystyle\sum_{i\neq m}{(y_i-\mu_i)(y_m-\mu_m)}}]$

$E[(y_i-\mu_i)(y_m-\mu_m)]$은 $i\neq m$일 때 0이다.

따라서 $\mathcal J_{jk}=\sigma^2 x_jx_k$ 를 유도할수 있다.


  ### 4.2.2. Quasi-newton method

newton-raphson법은 매 단계마다 Jacobian과 Hessian을 직접 구한다. 이것이 전체 솔루션에 과한 오버헤드가 될때 Quasi-newton method를 사용하는데, Hessian을 근사시키는 방법이면 모두 Quasi-newton method에 속한다.



#### 4.2.2.1. chord method

첫 번째 단계에서 구한 헤시안 $\mathbf H^{(0)}$을 매 단계에서 재사용

#### 4.2.2.2. Broyden–Fletcher–Goldfarb–Shanno algorithm

* <b>Initial step</b>

    1. 초기 파라미터 $\beta^{(0)}$를 설정
    2. $\mathbf H^{(0)}$설정.
    3. 스텝사이즈 $\alpha_0$ 입력받기

4. $p^{(0)}=-[\mathbf H^{(0)}]^{-1}U^{(0)}$
5. $s^{(0)}=p^{(0)}*\alpha^{(0)}$

6. $\beta^{(1)}=\beta^{(0)}+s^{(0)}$
7. $d^{(0)}=U^{(1)}-U^{(0)}$
8. $[H^{(1)}]^{-1}=[H^{(0)}]^{-1}+\frac{(s^{(0)T}d^{(0)}+d^{(0)T}H^{(0)-1}d^{(0)})(s^{(0)}s^{(0)T})}{(s^{(0)T}y^{(0)})^2}-\frac{H^{(0)-1}d^{(0)}s^{(0)T}+s^{(0)}d^{(0)T}H^{(0)-1}}{s^{(0)T}d^{(0)}}$



* <b>After first step, untill $\beta$ is converged</b>

9. $p^{(k)}=-[\mathbf H^{(k)}]^{-1}U^{(k)}$
10. $\alpha_k=argmin(f(x_k+\alpha_0))$
11. $s^{(k)}=p^{(k)}*\alpha^{(k)}$
12. $\beta^{(k+1)}=\beta^{(k)}+s^{(k)}$
13. $d^{(k)}=U^{(k+1)}-U^{(k)}$
14. $[H^{(k+1)}]^{-1}=[H^{(k)}]^{-1}+\frac{(s^{(k)T}d^{(k)}+d^{(k)T}H^{(k)-1}d^{(k)})(s^{(k)}s^{(k)T})}{(s^{(k)T}y^{(k)})^2}-\frac{H^{(k)-1}d^{(k)}s^{(k)T}+s^{(k)}d^{(k)T}H^{(k)-1}}{s^{(k)T}d^{(k)}}$



* <b>주의</b>

위 과정은 error minimizing 과정이다. 즉 likelihood function이 아닌 errofucntion에 대한 최적화.



* 증명 참고

https://en.wikipedia.org/wiki/Broyden–Fletcher–Goldfarb–Shanno_algorithm





#### 4.2.2.3. Limited-memory BFGS



$\beta$는 nx1 인 칼럼벡터이다. 이것의 Hessian은 nxn 크기의 행렬이라서 parameter의 차원이 메우

클경우 컴퓨터의 메모리 리소스를 많이 차지할 수 있다.



이에 대한 대안으로 Limited-memory BFGS는 m개의 저장된 특수 벡터들을 이용해서 $\mathbf H^{-1}*U$를 수행

한다.



 사용자가 기억할 벡터의 수 m을 설정하고 2개의 반복문으로 진행된다.



첫번째 반복문은 k-m에서 k까지의 기억벡터 q를 이용하여 z에변화를 적용하고



두번째 반복문은 보정하는듯하다.



더 연구 후 보충 예정