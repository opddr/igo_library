# 1. Usage
## 1.1 python

* <b>constructor</b>

<b>LogisticRegression(epoch, stepsize,method,verbose_training=False)</b>

epoch : the number of iterations.

stepsize : training accelerator.

method : chord, adam, <del>bfgs</del> and so on.

* <b>fitting</b>

<b>LogisticRegression(epoch, stepsize).fit(X,Y)</b>

X : 2-dimensional list. trainning dataset

Y : 1-dimensional list. label for each training data

* <b>predicting</b>

<b>LogisticRegression(epoch, stepsize).predict(test_data)</b>

test_data : 1-dimensional list to test



 * <b>example</b>
 
```
from igo_logistic_regression import LogisticRegression
from sklearn.datasets import *

skdata = load_digits()
test_data = [ 0,  0, 11, 10, 12,  4,  0,  0,  
0,  0, 12, 13,  9, 16,  1,  0,  
0,  0,  7, 13, 11, 16,  0,  0,  
0,  0,  1, 16, 15,  4,  0,  0,  
0,  0, 10, 16, 13,  0,  0,  0,  
0,  0, 14,  7, 12,  7,  0,  0,  
0,  4, 14,  4, 12, 13,  0,  0,  
0,  1, 11, 14, 12,  4,  0,  0]

#lr = LogisticRegression(30,5,"chord",True)
lr = LogisticRegression(30,5,"adam",False)

lr.fit(skdata["data"],skdata["target"])
print(lr.predict(test_data))


```

# 2. Algorithm

* Fitting

```
        self.X = X
        self.Y = Y
        self.targets = OrderedSet(Y)
        self.W = [[random.randint(-10,10) for r in range(0,len(X[0]))] for t in range(0,len(self.targets)) ] 

        for n,x in enumerate(X):

            Hessian = []
            iHessian = []

            for i in range(len(self.W[0])):
                Hessian.append([])
                for j in range(len(self.W[0])):
                    Hessian[i].append(x[i] * x[j])
            
            iHessian = np.linalg.pinv(np.array(Hessian))
           
            print("============================")
            for k in range(self.epoch) : 

                gradient = [0 for i in range(len(self.W[0]))] 
                for m in self.targets:
                    gradient_temp = []
                    eta = np.dot(np.array(x),np.array(self.W[m]))
                    theta = self.logistic_sigmoid(eta)
                    y=-1
         
                    if Y[n] == m:
                        y=1
                    else:
                        y=0                    
                    for i in range(len(self.W[m])):          
                        gradient_temp.append(x[i]*(y - theta))
                    print("epoch(",k,") error for target(",m,") : ",abs(y-theta))
                    gradient = gradient + gradient_temp
                
                gradient = np.array(gradient)
                training_unit = iHessian.dot(gradient)
                error_min = math.inf
                meetw = self.W[m]
                selected_step = 0
                    
                for step in range(1,self.step_size+1):
                    wtemp = np.add(self.W[m], step * training_unit)
                    error_temp = abs(y-self.logistic_sigmoid(np.dot(np.array(x),np.array(wtemp))))
                        
                    if error_min > error_temp:
                        meetw = wtemp
                        error_min = error_temp
                        selected_step = step
                self.W[m] = meetw
                print("epoch(",k,") ",m," target error : ",error_min,"... step : ",selected_step)


```

* Predicting

```
max = -math.inf
max_index = -1

for m in targets :
    eta = np.dot(x,self.W[m])
    theta = self.logistic_sigmoid(eta)
    if theta > max
        max = theta
        max_index = -1
        
return max_index
    
```


# 3. report
실험데이터 
* classification : scikit-learn iris dataset
* image recognition : scikit-learn digits

## 3.1 Iris classification

### 3.1.1 분류 정확도 

 sample이 많을 수록 predict 정확도 상승
 
1000개정도면 양호한 성능 

```
dataset :  [5.1 3.5 1.4 0.2]
label :  0
[5.1 3.5 1.4 0.2]
[-0.10060325 -3.71310157  4.28652006  1.64677797]
-7.178448382270953
predict :  0.0007622690794102059
============================
dataset :  [5.4 3.7 1.5 0.2]
label :  0
[5.4 3.7 1.5 0.2]
[-0.10060325 -3.71310157  4.28652006  1.64677797]
-7.522597664673023
predict :  0.0005404338880496632
============================
dataset :  [5.4 3.4 1.7 0.2]
label :  0
[5.4 3.4 1.7 0.2]
[-0.10060325 -3.71310157  4.28652006  1.64677797]
-5.551363182387519
predict :  0.0038671486448136347
============================
dataset :  [4.8 3.1 1.6 0.2]
label :  0
[4.8 3.1 1.6 0.2]
[-0.10060325 -3.71310157  4.28652006  1.64677797]
-4.805722767979498
predict :  0.00811637011328619
============================
dataset :  [5.  3.5 1.3 0.3]
label :  0
[5.  3.5 1.3 0.3]
[-0.10060325 -3.71310157  4.28652006  1.64677797]
-7.432362266441215
predict :  0.0005914378929472296
============================
dataset :  [7.  3.2 4.7 1.4]
label :  1
[7.  3.2 4.7 1.4]
[-0.10060325 -3.71310157  4.28652006  1.64677797]
9.865985678372699
predict :  0.9999480919974375
============================
dataset :  [5.  2.  3.5 1. ]
label :  1
[5.  2.  3.5 1. ]
[-0.10060325 -3.71310157  4.28652006  1.64677797]
8.720378798052677
predict :  0.9998368012884016
============================
dataset :  [5.9 3.2 4.8 1.8]
label :  1
[5.9 3.2 4.8 1.8]
[-0.10060325 -3.71310157  4.28652006  1.64677797]
11.06401244869974
predict :  0.9999843341616039
============================
dataset :  [5.5 2.4 3.8 1.1]
label :  1
[5.5 2.4 3.8 1.1]
[-0.10060325 -3.71310157  4.28652006  1.64677797]
8.635470361517136
predict :  0.9998223416147557
============================
dataset :  [5.5 2.6 4.4 1.2]
label :  1
[5.5 2.6 4.4 1.2]
[-0.10060325 -3.71310157  4.28652006  1.64677797]
10.629439882521655
predict :  0.9999758074077106
============================
```

### 3.1.2 에러 감소추이

##### step size = 1 
```
epoch( 0 )  0  target error :  0.9999984848590281 ... step :  1
epoch( 0 )  1  target error :  0.004476192359636612 ... step :  1
epoch( 0 )  2  target error :  1.0 ... step :  1
epoch( 1 )  0  target error :  0.999995881436791 ... step :  1
epoch( 1 )  1  target error :  0.004456289925940996 ... step :  1
epoch( 1 )  2  target error :  1.0 ... step :  1
epoch( 2 )  0  target error :  0.9999888047098054 ... step :  1
epoch( 2 )  1  target error :  0.004436563495612599 ... step :  1
epoch( 2 )  2  target error :  1.0 ... step :  1
epoch( 3 )  0  target error :  0.9999695689721735 ... step :  1
epoch( 3 )  1  target error :  0.004417010745986157 ... step :  1
epoch( 3 )  2  target error :  1.0 ... step :  1
epoch( 4 )  0  target error :  0.9999172867320502 ... step :  1
epoch( 4 )  1  target error :  0.00439762939506327 ... step :  1
epoch( 4 )  2  target error :  1.0 ... step :  1
epoch( 5 )  0  target error :  0.9997752125667414 ... step :  1
epoch( 5 )  1  target error :  0.004378417200626748 ... step :  1
epoch( 5 )  2  target error :  1.0 ... step :  1
epoch( 6 )  0  target error :  0.9993893375259849 ... step :  1
epoch( 6 )  1  target error :  0.004359371959377804 ... step :  1
epoch( 6 )  2  target error :  1.0 ... step :  1
epoch( 7 )  0  target error :  0.9983427978616194 ... step :  1
epoch( 7 )  1  target error :  0.004340491506095727 ... step :  1
epoch( 7 )  2  target error :  1.0 ... step :  1
epoch( 8 )  0  target error :  0.9955154531051569 ... step :  1
epoch( 8 )  1  target error :  0.004321773712819174 ... step :  1
epoch( 8 )  2  target error :  1.0 ... step :  1
epoch( 9 )  0  target error :  0.9879564304711008 ... step :  1
epoch( 9 )  1  target error :  0.0043032164880485325 ... step :  1
epoch( 9 )  2  target error :  1.0 ... step :  1
epoch( 10 )  0  target error :  0.9682977292019301 ... step :  1
epoch( 10 )  1  target error :  0.004284817775968636 ... step :  1
epoch( 10 )  2  target error :  1.0 ... step :  1
epoch( 11 )  0  target error :  0.9206238483324028 ... step :  1
epoch( 11 )  1  target error :  0.004266575555691258 ... step :  1
epoch( 11 )  2  target error :  1.0 ... step :  1
epoch( 12 )  0  target error :  0.8220399227412443 ... step :  1
epoch( 12 )  1  target error :  0.0042484878405168805 ... step :  1
epoch( 12 )  2  target error :  1.0 ... step :  1
epoch( 13 )  0  target error :  0.670001048223101 ... step :  1
epoch( 13 )  1  target error :  0.004230552677215028 ... step :  1
epoch( 13 )  2  target error :  1.0 ... step :  1
epoch( 14 )  0  target error :  0.5095460275457562 ... step :  1
epoch( 14 )  1  target error :  0.004212768145322689 ... step :  1
epoch( 14 )  2  target error :  1.0 ... step :  1
epoch( 15 )  0  target error :  0.3842950447221094 ... step :  1
epoch( 15 )  1  target error :  0.004195132356460463 ... step :  1
epoch( 15 )  2  target error :  1.0 ... step :  1
epoch( 16 )  0  target error :  0.29824849989714386 ... step :  1
epoch( 16 )  1  target error :  0.004177643453665523 ... step :  1
epoch( 16 )  2  target error :  1.0 ... step :  1
epoch( 17 )  0  target error :  0.23977730351154292 ... step :  1
epoch( 17 )  1  target error :  0.004160299610741325 ... step :  1
epoch( 17 )  2  target error :  1.0 ... step :  1
epoch( 18 )  0  target error :  0.19882121429329902 ... step :  1
epoch( 18 )  1  target error :  0.0041430990316233315 ... step :  1
epoch( 18 )  2  target error :  1.0 ... step :  1
epoch( 19 )  0  target error :  0.16903254791649303 ... step :  1
epoch( 19 )  1  target error :  0.004126039949760302 ... step :  1
epoch( 19 )  2  target error :  1.0 ... step :  1
epoch( 20 )  0  target error :  0.14659858553787897 ... step :  1
epoch( 20 )  1  target error :  0.0041091206275108896 ... step :  1
epoch( 20 )  2  target error :  1.0 ... step :  1
epoch( 21 )  0  target error :  0.12919102951771888 ... step :  1
epoch( 21 )  1  target error :  0.004092339355554827 ... step :  1
epoch( 21 )  2  target error :  0.9999999999999998 ... step :  1
epoch( 22 )  0  target error :  0.1153397551042612 ... step :  1
epoch( 22 )  1  target error :  0.004075694452318657 ... step :  1
epoch( 22 )  2  target error :  0.9999999999999993 ... step :  1
epoch( 23 )  0  target error :  0.10408280040750806 ... step :  1
epoch( 23 )  1  target error :  0.004059184263415024 ... step :  1
epoch( 23 )  2  target error :  0.9999999999999982 ... step :  1
epoch( 24 )  0  target error :  0.09476932173760633 ... step :  1
epoch( 24 )  1  target error :  0.004042807161095901 ... step :  1
epoch( 24 )  2  target error :  0.9999999999999953 ... step :  1
epoch( 25 )  0  target error :  0.08694555612687838 ... step :  1
epoch( 25 )  1  target error :  0.004026561543718666 ... step :  1
epoch( 25 )  2  target error :  0.9999999999999873 ... step :  1
epoch( 26 )  0  target error :  0.08028663184731533 ... step :  1
epoch( 26 )  1  target error :  0.0040104458352251265 ... step :  1
epoch( 26 )  2  target error :  0.9999999999999656 ... step :  1
epoch( 27 )  0  target error :  0.07455444991675808 ... step :  1
epoch( 27 )  1  target error :  0.003994458484632836 ... step :  1
epoch( 27 )  2  target error :  0.9999999999999065 ... step :  1
epoch( 28 )  0  target error :  0.06957086657896261 ... step :  1
epoch( 28 )  1  target error :  0.003978597965538643 ... step :  1
epoch( 28 )  2  target error :  0.9999999999997455 ... step :  1
epoch( 29 )  0  target error :  0.06520013711833816 ... step :  1
epoch( 29 )  1  target error :  0.003962862775633827 ... step :  1
epoch( 29 )  2  target error :  0.9999999999993086 ... step :  1
epoch( 30 )  0  target error :  0.06133712962747673 ... step :  1
epoch( 30 )  1  target error :  0.003947251436230853 ... step :  1
epoch( 30 )  2  target error :  0.9999999999981204 ... step :  1
epoch( 31 )  0  target error :  0.057899230377556066 ... step :  1
epoch( 31 )  1  target error :  0.003931762491800993 ... step :  1
epoch( 31 )  2  target error :  0.999999999994891 ... step :  1
epoch( 32 )  0  target error :  0.05482066911579753 ... step :  1
epoch( 32 )  1  target error :  0.003916394509522949 ... step :  1
epoch( 32 )  2  target error :  0.999999999986112 ... step :  1
epoch( 33 )  0  target error :  0.05204846616090919 ... step :  1
epoch( 33 )  1  target error :  0.0039011460788419684 ... step :  1
epoch( 33 )  2  target error :  0.9999999999622486 ... step :  1
epoch( 34 )  0  target error :  0.04953948853111201 ... step :  1
epoch( 34 )  1  target error :  0.0038860158110391387 ... step :  1
epoch( 34 )  2  target error :  0.9999999998973812 ... step :  1
epoch( 35 )  0  target error :  0.047258278534877296 ... step :  1
epoch( 35 )  1  target error :  0.003871002338810586 ... step :  1
epoch( 35 )  2  target error :  0.9999999997210531 ... step :  1
epoch( 36 )  0  target error :  0.04517542951788622 ... step :  1
epoch( 36 )  1  target error :  0.0038561043158564863 ... step :  1
epoch( 36 )  2  target error :  0.9999999992417439 ... step :  1
epoch( 37 )  0  target error :  0.043266355192466355 ... step :  1
epoch( 37 )  1  target error :  0.0038413204164794443 ... step :  1
epoch( 37 )  2  target error :  0.9999999979388463 ... step :  1
epoch( 38 )  0  target error :  0.04151034611620863 ... step :  1
epoch( 38 )  1  target error :  0.003826649335192032 ... step :  1
epoch( 38 )  2  target error :  0.9999999943972036 ... step :  1
epoch( 39 )  0  target error :  0.0398898384181412 ... step :  1
epoch( 39 )  1  target error :  0.003812089786333289 ... step :  1
epoch( 39 )  2  target error :  0.9999999847700207 ... step :  1
epoch( 40 )  0  target error :  0.038389841309628525 ... step :  1
epoch( 40 )  1  target error :  0.00379764050369389 ... step :  1
epoch( 40 )  2  target error :  0.9999999586006255 ... step :  1
epoch( 41 )  0  target error :  0.036997484716159046 ... step :  1
epoch( 41 )  1  target error :  0.003783300240149854 ... step :  1
epoch( 41 )  2  target error :  0.9999998874648452 ... step :  1
epoch( 42 )  0  target error :  0.03570165872647513 ... step :  1
epoch( 42 )  1  target error :  0.00376906776730439 ... step :  1
epoch( 42 )  2  target error :  0.9999996940978276 ... step :  1
epoch( 43 )  0  target error :  0.03449272390389391 ... step :  1
epoch( 43 )  1  target error :  0.003754941875137882 ... step :  1
epoch( 43 )  2  target error :  0.9999991684723748 ... step :  1
epoch( 44 )  0  target error :  0.033362276780701694 ... step :  1
epoch( 44 )  1  target error :  0.003740921371665519 ... step :  1
epoch( 44 )  2  target error :  0.9999977396786754 ... step :  1
epoch( 45 )  0  target error :  0.032302958687988714 ... step :  1
epoch( 45 )  1  target error :  0.0037270050826028476 ... step :  1
epoch( 45 )  2  target error :  0.9999938558473679 ... step :  1
epoch( 46 )  0  target error :  0.03130829888554765 ... step :  1
epoch( 46 )  1  target error :  0.003713191851038446 ... step :  1
epoch( 46 )  2  target error :  0.9999832987404852 ... step :  1
epoch( 47 )  0  target error :  0.030372585041318234 ... step :  1
epoch( 47 )  1  target error :  0.0036994805371139868 ... step :  1
epoch( 47 )  2  target error :  0.9999546033306932 ... step :  1
epoch( 48 )  0  target error :  0.029490755670019353 ... step :  1
epoch( 48 )  1  target error :  0.00368587001771123 ... step :  1
epoch( 48 )  2  target error :  0.9998766142845366 ... step :  1
epoch( 49 )  0  target error :  0.028658310318510072 ... step :  1
epoch( 49 )  1  target error :  0.0036723591861460886 ... step :  1
epoch( 49 )  2  target error :  0.9996647153027433 ... step :  1
```


##### stepsize = 5
```
epoch( 0 )  0  target error :  1.0 ... step :  1
epoch( 0 )  1  target error :  0.9999999924370161 ... step :  4
epoch( 0 )  2  target error :  0.9999996619257742 ... step :  4
epoch( 1 )  0  target error :  1.0 ... step :  1
epoch( 1 )  1  target error :  0.9999995870752416 ... step :  4
epoch( 1 )  2  target error :  0.9999815421321215 ... step :  4
epoch( 2 )  0  target error :  1.0 ... step :  1
epoch( 2 )  1  target error :  0.9999774556082798 ... step :  4
epoch( 2 )  2  target error :  0.9989933048186053 ... step :  4
epoch( 3 )  0  target error :  1.0 ... step :  1
epoch( 3 )  1  target error :  0.9987707141673614 ... step :  4
epoch( 3 )  2  target error :  0.9480488407784554 ... step :  4
epoch( 4 )  0  target error :  1.0 ... step :  1
epoch( 4 )  1  target error :  0.9373215675511528 ... step :  4
epoch( 4 )  2  target error :  0.2915034447892226 ... step :  4
epoch( 5 )  0  target error :  1.0 ... step :  1
epoch( 5 )  1  target error :  0.2603255359448618 ... step :  4
epoch( 5 )  2  target error :  0.11363803370593095 ... step :  4
epoch( 6 )  0  target error :  1.0 ... step :  1
epoch( 6 )  1  target error :  0.11050634727593463 ... step :  4
epoch( 6 )  2  target error :  0.07525335691466853 ... step :  4
epoch( 7 )  0  target error :  1.0 ... step :  1
epoch( 7 )  1  target error :  0.0739454967912969 ... step :  4
epoch( 7 )  2  target error :  0.05680371320449138 ... step :  4
epoch( 8 )  0  target error :  1.0 ... step :  1
epoch( 8 )  1  target error :  0.0560734039829181 ... step :  4
epoch( 8 )  2  target error :  0.0457869624534096 ... step :  4
epoch( 9 )  0  target error :  0.9999999999999999 ... step :  4
epoch( 9 )  1  target error :  0.045317722823637226 ... step :  4
epoch( 9 )  2  target error :  0.038418677039726526 ... step :  4
epoch( 10 )  0  target error :  0.9999999999999969 ... step :  4
epoch( 10 )  1  target error :  0.03809065279152557 ... step :  4
epoch( 10 )  2  target error :  0.03312726798253413 ... step :  4
epoch( 11 )  0  target error :  0.9999999999998295 ... step :  4
epoch( 11 )  1  target error :  0.032884582768093584 ... step :  4
epoch( 11 )  2  target error :  0.029135792811699413 ... step :  4
epoch( 12 )  0  target error :  0.9999999999906907 ... step :  4
epoch( 12 )  1  target error :  0.02894874960723444 ... step :  4
epoch( 12 )  2  target error :  0.026013992543816807 ... step :  4
epoch( 13 )  0  target error :  0.9999999994917258 ... step :  4
epoch( 13 )  1  target error :  0.025865301334159183 ... step :  4
epoch( 13 )  2  target error :  0.02350357987488704 ... step :  4
epoch( 14 )  0  target error :  0.9999999722491684 ... step :  4
epoch( 14 )  1  target error :  0.02338247148663955 ... step :  4
epoch( 14 )  2  target error :  0.021439813442994365 ... step :  4
epoch( 15 )  0  target error :  0.9999984848583549 ... step :  4
epoch( 15 )  1  target error :  0.021339221376386032 ... step :  4
epoch( 15 )  2  target error :  0.019712519918977444 ... step :  4
epoch( 16 )  0  target error :  0.999917283287771 ... step :  4
epoch( 16 )  1  target error :  0.0196276105636792 ... step :  4
epoch( 16 )  2  target error :  0.018245156264593224 ... step :  4
epoch( 17 )  0  target error :  0.99550523519948 ... step :  4
epoch( 17 )  1  target error :  0.018172509186007994 ... step :  4
epoch( 17 )  2  target error :  0.016982848958697273 ... step :  4
epoch( 18 )  0  target error :  0.8050742982282544 ... step :  4
epoch( 18 )  1  target error :  0.016919974311674538 ... step :  4
epoch( 18 )  2  target error :  0.015885210713253876 ... step :  4
epoch( 19 )  0  target error :  0.14161007879446585 ... step :  4
epoch( 19 )  1  target error :  0.015830252149862556 ... step :  4
epoch( 19 )  2  target error :  0.01492184323943496 ... step :  4
epoch( 20 )  0  target error :  0.0856126356878616 ... step :  4
epoch( 20 )  1  target error :  0.014873388012072018 ... step :  4
epoch( 20 )  2  target error :  0.014069420961876218 ... step :  4
epoch( 21 )  0  target error :  0.06233486705189162 ... step :  4
epoch( 21 )  1  target error :  0.014026374592122436 ... step :  4
epoch( 21 )  2  target error :  0.013309742092841 ... step :  4
epoch( 22 )  0  target error :  0.04925610438450356 ... step :  4
epoch( 22 )  1  target error :  0.013271243332325714 ... step :  4
epoch( 22 )  2  target error :  0.012628391716465731 ... step :  4
epoch( 23 )  0  target error :  0.040807110917061795 ... step :  4
epoch( 23 )  1  target error :  0.012593753435692934 ... step :  4
epoch( 23 )  2  target error :  0.012013803473300715 ... step :  4
epoch( 24 )  0  target error :  0.034875777724490686 ... step :  4
epoch( 24 )  1  target error :  0.011982470689514387 ... step :  4
epoch( 24 )  2  target error :  0.011456587555058186 ... step :  4
epoch( 25 )  0  target error :  0.030472996649354434 ... step :  4
epoch( 25 )  1  target error :  0.011428107061418459 ... step :  4
epoch( 25 )  2  target error :  0.010949040686791244 ... step :  4
epoch( 26 )  0  target error :  0.02707070203959605 ... step :  4
epoch( 26 )  1  target error :  0.010923038701913567 ... step :  4
epoch( 26 )  2  target error :  0.010484783002708142 ... step :  4
epoch( 27 )  0  target error :  0.02436021477607342 ... step :  4
epoch( 27 )  1  target error :  0.010460948470764505 ... step :  4
epoch( 27 )  2  target error :  0.010058485014692264 ... step :  4
epoch( 28 )  0  target error :  0.022148608259333735 ... step :  4
epoch( 28 )  1  target error :  0.010036556954889158 ... step :  4
epoch( 28 )  2  target error :  0.009665659598178875 ... step :  4
epoch( 29 )  0  target error :  0.02030891245364108 ... step :  4
epoch( 29 )  1  target error :  0.00964541740128645 ... step :  4
epoch( 29 )  2  target error :  0.009302501599672686 ... step :  4
epoch( 30 )  0  target error :  0.01875405523424334 ... step :  4
epoch( 30 )  1  target error :  0.009283757499704138 ... step :  4
epoch( 30 )  2  target error :  0.008965762799025648 ... step :  4
epoch( 31 )  0  target error :  0.017422284261339738 ... step :  4
epoch( 31 )  1  target error :  0.008948355971101378 ... step :  4
epoch( 31 )  2  target error :  0.008652653446243814 ... step :  4
epoch( 32 )  0  target error :  0.016268560575415814 ... step :  4
epoch( 32 )  1  target error :  0.008636445334644484 ... step :  4
epoch( 32 )  2  target error :  0.008360764001652664 ... step :  4
epoch( 33 )  0  target error :  0.015259247048105107 ... step :  4
epoch( 33 )  1  target error :  0.008345634588659681 ... step :  4
epoch( 33 )  2  target error :  0.008088002397752857 ... step :  4
epoch( 34 )  0  target error :  0.01436870570993476 ... step :  4
epoch( 34 )  1  target error :  0.00807384719921782 ... step :  4
epoch( 34 )  2  target error :  0.007832543342346501 ... step :  4
epoch( 35 )  0  target error :  0.01357704747148869 ... step :  4
epoch( 35 )  1  target error :  0.00781927096988647 ... step :  4
epoch( 35 )  2  target error :  0.0075927870475173374 ... step :  4
epoch( 36 )  0  target error :  0.01286860249376287 ... step :  4
epoch( 36 )  1  target error :  0.0075803172163455956 ... step :  4
epoch( 36 )  2  target error :  0.007367325399301079 ... step :  4
epoch( 37 )  0  target error :  0.012230855179701794 ... step :  4
epoch( 37 )  1  target error :  0.007355587289377186 ... step :  4
epoch( 37 )  2  target error :  0.007154914047144095 ... step :  4
epoch( 38 )  0  target error :  0.011653686825642606 ... step :  4
epoch( 38 )  1  target error :  0.007143844946571424 ... step :  4
epoch( 38 )  2  target error :  0.006954449237746478 ... step :  4
epoch( 39 )  0  target error :  0.011128826857035756 ... step :  4
epoch( 39 )  1  target error :  0.006943993413239731 ... step :  4
epoch( 39 )  2  target error :  0.006764948477462669 ... step :  4
epoch( 40 )  0  target error :  0.010649448475736456 ... step :  4
epoch( 40 )  1  target error :  0.006755056228709734 ... step :  4
epoch( 40 )  2  target error :  0.006585534304208152 ... step :  4
epoch( 41 )  0  target error :  0.010209866185857286 ... step :  4
epoch( 41 )  1  target error :  0.006576161168091268 ... step :  4
epoch( 41 )  2  target error :  0.006415420600243257 ... step :  4
epoch( 42 )  0  target error :  0.009805306419667925 ... step :  4
epoch( 42 )  1  target error :  0.006406526677903479 ... step :  4
epoch( 42 )  2  target error :  0.006253900993106963 ... step :  4
epoch( 43 )  0  target error :  0.009431731425257106 ... step :  4
epoch( 43 )  1  target error :  0.006245450378265456 ... step :  4
epoch( 43 )  2  target error :  0.006100338981940268 ... step :  4
epoch( 44 )  0  target error :  0.009085702507344862 ... step :  4
epoch( 44 )  1  target error :  0.0060922992731224776 ... step :  4
epoch( 44 )  2  target error :  0.005954159496768905 ... step :  4
epoch( 45 )  0  target error :  0.008764272718440158 ... step :  4
epoch( 45 )  1  target error :  0.005946501379397033 ... step :  4
epoch( 45 )  2  target error :  0.005814841653657859 ... step :  4
epoch( 46 )  0  target error :  0.008464901849406958 ... step :  4
epoch( 46 )  1  target error :  0.005807538540599668 ... step :  4
epoch( 46 )  2  target error :  0.005681912512476298 ... step :  4
epoch( 47 )  0  target error :  0.008185388488246681 ... step :  4
epoch( 47 )  1  target error :  0.005674940233721093 ... step :  4
epoch( 47 )  2  target error :  0.005554941678923875 ... step :  4
epoch( 48 )  0  target error :  0.00792381527420205 ... step :  4
epoch( 48 )  1  target error :  0.005548278212720948 ... step :  4
epoch( 48 )  2  target error :  0.005433536620438494 ... step :  4
epoch( 49 )  0  target error :  0.007678504447976464 ... step :  4
epoch( 49 )  1  target error :  0.005427161859571546 ... step :  4
epoch( 49 )  2  target error :  0.00531733858813546 ... step :  4

```



## 3.2 Digits image recognition

* 분류 정확도
```
============================
dataset :  [ 0.  0.  5. 13.  9.  1.  0.  0.  0.  0. 13. 15. 10. 15.  5.  0.  0.  3.
 15.  2.  0. 11.  8.  0.  0.  4. 12.  0.  0.  8.  8.  0.  0.  5.  8.  0.
  0.  9.  8.  0.  0.  4. 11.  0.  1. 12.  7.  0.  0.  2. 14.  5. 10. 12.
  0.  0.  0.  0.  6. 13. 10.  0.  0.  0.]
label :  0
predict :  (0, 1.0)
============================
dataset :  [ 0.  0.  1.  9. 15. 11.  0.  0.  0.  0. 11. 16.  8. 14.  6.  0.  0.  2.
 16. 10.  0.  9.  9.  0.  0.  1. 16.  4.  0.  8.  8.  0.  0.  4. 16.  4.
  0.  8.  8.  0.  0.  1. 16.  5.  1. 11.  3.  0.  0.  0. 12. 12. 10. 10.
  0.  0.  0.  0.  1. 10. 13.  3.  0.  0.]
label :  0
predict :  (0, 1.0)
============================
dataset :  [ 0.  0.  3. 13. 11.  7.  0.  0.  0.  0. 11. 16. 16. 16.  2.  0.  0.  4.
 16.  9.  1. 14.  2.  0.  0.  4. 16.  0.  0. 16.  2.  0.  0.  0. 16.  1.
  0. 12.  8.  0.  0.  0. 15.  9.  0. 13.  6.  0.  0.  0.  9. 14.  9. 14.
  1.  0.  0.  0.  2. 12. 13.  4.  0.  0.]
label :  0
predict :  (0, 1.0)
============================
dataset :  [ 0.  0. 10. 14. 11.  3.  0.  0.  0.  4. 16. 13.  6. 14.  1.  0.  0.  4.
 16.  2.  0. 11.  7.  0.  0.  8. 16.  0.  0. 10.  5.  0.  0.  8. 16.  0.
  0. 14.  4.  0.  0.  8. 16.  0.  1. 16.  1.  0.  0.  4. 16.  1. 11. 15.
  0.  0.  0.  0. 11. 16. 12.  3.  0.  0.]
label :  0
predict :  (0, 1.0)
============================
dataset :  [ 0.  0. 11. 10. 12.  4.  0.  0.  0.  0. 12. 13.  9. 16.  1.  0.  0.  0.
  7. 13. 11. 16.  0.  0.  0.  0.  1. 16. 14.  4.  0.  0.  0.  0. 10. 16.
 13.  0.  0.  0.  0.  0. 14.  7. 12.  7.  0.  0.  0.  4. 14.  4. 12. 13.
  0.  0.  0.  1. 11. 14. 12.  4.  0.  0.]
label :  8
predict :  (8, 1.0)
============================
dataset :  [ 0.  0.  0.  5. 14. 12.  2.  0.  0.  0.  7. 15.  8. 14.  4.  0.  0.  0.
  6.  2.  3. 13.  1.  0.  0.  0.  0.  1. 13.  4.  0.  0.  0.  0.  1. 11.
  9.  0.  0.  0.  0.  8. 16. 13.  0.  0.  0.  0.  0.  5. 14. 16. 11.  2.
  0.  0.  0.  0.  0.  6. 12. 13.  3.  0.]
label :  2
predict :  (8, 1.0)
============================
dataset :  [ 0.  0. 10. 15. 14.  4.  0.  0.  0.  0.  4.  6. 13. 16.  2.  0.  0.  0.
  0.  3. 16.  9.  0.  0.  0.  0.  0.  1. 16.  6.  0.  0.  0.  0.  0.  0.
 10. 12.  0.  0.  0.  0.  0.  0.  1. 16.  4.  0.  0.  1.  9.  5.  6. 16.
  7.  0.  0.  0. 14. 12. 15. 11.  2.  0.]
label :  3
predict :  (2, 1.0)
============================
dataset :  [ 0.  0.  0.  0. 14.  7.  0.  0.  0.  0.  0. 13. 16.  9.  0.  0.  0.  0.
 10. 16. 16.  7.  0.  0.  0.  7. 16.  8. 16.  2.  0.  0.  0.  1.  5.  6.
 16.  6.  0.  0.  0.  0.  0.  4. 16.  6.  0.  0.  0.  0.  0.  2. 16.  6.
  0.  0.  0.  0.  0.  0. 12. 11.  0.  0.]
label :  1
predict :  (1, 1.0)
============================
dataset :  [ 0.  0.  0.  1. 14. 13.  1.  0.  0.  0.  0.  1. 16. 16.  3.  0.  0.  5.
 11. 15. 16. 16.  0.  0.  0.  4. 15. 16. 16. 15.  0.  0.  0.  0.  0.  8.
 16.  7.  0.  0.  0.  0.  0. 10. 16.  3.  0.  0.  0.  0.  0.  8. 16.  6.
  0.  0.  0.  0.  0.  2. 13. 15.  2.  0.]
label :  1
predict :  (1, 1.0)
============================
dataset :  [ 0.  0.  0.  1. 12.  8.  1.  0.  0.  0.  0.  4. 16. 16.  1.  0.  0.  0.
  1. 13. 16. 11.  0.  0.  0.  1. 11. 16. 16. 12.  0.  0.  0.  2. 12.  8.
 16. 10.  0.  0.  0.  0.  0.  0. 15.  8.  0.  0.  0.  0.  0.  4. 16.  4.
  0.  0.  0.  0.  0.  3. 13.  4.  0.  0.]
label :  1
predict :  (1, 1.0)
============================
dataset :  [ 0.  0.  0.  2. 13.  0.  0.  0.  0.  0.  0.  8. 15.  0.  0.  0.  0.  0.
  5. 16.  5.  2.  0.  0.  0.  0. 15. 12.  1. 16.  4.  0.  0.  4. 16.  2.
  9. 16.  8.  0.  0.  0. 10. 14. 16. 16.  4.  0.  0.  0.  0.  0. 13.  8.
  0.  0.  0.  0.  0.  0. 13.  6.  0.  0.]
label :  4
predict :  (4, 0.9999999999986513)
============================
dataset :  [ 0.  0.  0. 10. 15.  0.  0.  0.  0.  0. 11. 15.  3.  0.  0.  0.  0.  7.
 15.  4.  0.  0.  0.  0.  0. 12. 11.  1.  3.  8.  2.  0.  0.  4. 12. 15.
 15. 16.  9.  0.  0.  0.  0.  8. 16.  8.  2.  0.  0.  0.  0. 10. 12.  0.
  0.  0.  0.  0.  0. 12.  9.  0.  0.  0.]
label :  4
predict :  (4, 1.0)
============================
dataset :  [ 0.  0.  6. 13.  2.  0.  0.  0.  0.  4. 16. 16. 16. 11.  0.  0.  0.  0.
 12. 11.  1.  6.  1.  0.  0.  0. 12. 14. 10.  2.  0.  0.  0.  0.  1.  8.
 12. 12.  0.  0.  0.  0.  0.  0.  9. 14.  0.  0.  0.  0.  4.  9. 16.  5.
  0.  0.  0.  0.  9. 14.  4.  0.  0.  0.]
label :  5
predict :  (5, 0.9999999999999991)
============================
dataset :  [ 0.  0.  2. 12.  9.  0.  0.  0.  0.  0. 11. 15. 12.  5.  0.  0.  0.  0.
 15.  5.  0. 14.  0.  0.  0.  2. 15.  1.  0.  9.  7.  0.  0.  4. 10.  0.
  0.  7.  8.  0.  0.  0. 12.  0.  0.  8. 10.  0.  0.  2. 15.  5. 10. 16.
  1.  0.  0.  0.  5. 14. 12.  4.  0.  0.]
label :  0
predict :  (0, 1.0)
============================
dataset :  [ 0.  0. 12.  9. 12.  1.  0.  0.  0.  0. 14. 16. 16.  8.  0.  0.  0.  3.
 16.  9.  3. 15.  2.  0.  0.  4. 16.  1.  0. 16.  5.  0.  0.  5. 12.  0.
  0. 16.  5.  0.  0.  3. 14.  1.  4. 16.  4.  0.  0.  0. 15. 12. 14. 14.
  0.  0.  0.  0.  7. 12. 12.  2.  0.  0.]
label :  0
predict :  (0, 0.999874441637863)
============================
dataset :  [ 0.  0.  2. 12.  4.  0.  0.  0.  0.  1. 12. 16. 16.  3.  0.  0.  0.  7.
 16.  6.  4. 13.  0.  0.  0.  8. 16.  6.  0. 13.  5.  0.  0.  1. 16.  5.
  0.  7.  9.  0.  0.  0. 16.  8.  0.  8. 12.  0.  0.  0. 13. 14. 14. 16.
 10.  0.  0.  0.  4. 14. 15.  7.  0.  0.]
label :  0
predict :  (0, 1.0)
============================
dataset :  [ 0.  0.  5. 14. 10.  7.  0.  0.  0.  0. 16. 16. 16. 16.  3.  0.  0.  3.
 16. 10.  2. 16.  7.  0.  0.  7. 16.  3.  0. 12.  8.  0.  0.  8. 16.  1.
  0. 12.  8.  0.  0.  7. 16.  5.  2. 16.  4.  0.  0.  2. 16. 15. 14. 13.
  0.  0.  0.  0.  7. 15. 13.  2.  0.  0.]
label :  0
predict :  (0, 1.0)
============================
dataset :  [ 0.  0.  2. 13.  4.  0.  0.  0.  0.  0.  8. 14. 11.  0.  0.  0.  0.  0.
 10.  6. 14.  5.  2.  0.  0.  0.  2. 14. 12. 14.  0.  0.  0.  0.  1. 15.
 13.  2.  0.  0.  0.  0. 11. 13. 14.  1.  0.  0.  0.  0. 13.  8. 10.  4.
  0.  0.  0.  0.  2. 11. 16.  7.  0.  0.]
label :  8
predict :  (1, 1.0)
============================
dataset :  [ 0.  0.  7. 15. 15.  4.  0.  0.  0.  8. 16. 16. 16.  4.  0.  0.  0.  8.
 15.  8. 16.  4.  0.  0.  0.  0.  0. 10. 15.  0.  0.  0.  0.  0.  1. 15.
  9.  0.  0.  0.  0.  0.  6. 16.  2.  0.  0.  0.  0.  0.  8. 16.  8. 11.
  9.  0.  0.  0.  9. 16. 16. 12.  3.  0.]
label :  2
predict :  (2, 3.627420066830687e-13)
============================
dataset :  [ 0.  2. 15. 15.  6.  0.  0.  0.  0.  0. 10. 13. 16.  5.  0.  0.  0.  0.
  0.  2. 16.  9.  0.  0.  0.  0.  0.  3. 16. 11.  0.  0.  0.  0.  0.  0.
 13. 14.  1.  0.  0.  0.  0.  0.  7. 16.  5.  0.  0.  1.  4.  6. 13. 15.
  1.  0.  0.  3. 15. 14. 11.  2.  0.  0.]
label :  3
predict :  (3, 1.0)
============================
dataset :  [ 0.  0.  0.  0. 11. 12.  0.  0.  0.  0.  0.  3. 15. 14.  0.  0.  0.  0.
  0. 11. 16. 11.  0.  0.  0.  0.  9. 16. 16. 10.  0.  0.  0.  4. 16. 12.
 16. 12.  0.  0.  0.  3. 10.  3. 16. 11.  0.  0.  0.  0.  0.  0. 16. 14.
  0.  0.  0.  0.  0.  0. 11. 11.  0.  0.]
label :  1
predict :  (1, 1.0)
============================
dataset :  [ 0.  0.  0.  0.  7. 14.  7.  0.  0.  0.  0.  3. 16. 16.  9.  0.  0.  0.
  6. 15. 16. 16.  6.  0.  0.  5. 16. 16. 16. 16.  2.  0.  0.  4.  8.  8.
 16. 16.  0.  0.  0.  0.  0.  3. 16. 16.  3.  0.  0.  0.  0.  1. 13. 16.
  0.  0.  0.  0.  0.  0.  8. 15.  0.  0.]
label :  1
predict :  (1, 0.765111386252179)
============================
dataset :  [ 0.  0. 11. 16.  8.  0.  0.  0.  0.  6. 16. 11. 13.  9.  0.  0.  0.  7.
 16.  0.  9. 16.  0.  0.  0.  2. 15. 12. 16. 16.  3.  0.  0.  0.  5.  7.
  7. 16.  4.  0.  0.  0.  0.  0.  5. 16.  5.  0.  0.  0.  3.  7. 16. 11.
  0.  0.  0.  0. 13. 16. 11.  1.  0.  0.]
label :  9
predict :  (9, 6.902386882399244e-09)
============================
dataset :  [ 0.  3. 15. 13. 12.  8.  1.  0.  0.  4. 16. 14. 12. 12.  2.  0.  0.  0.
 16.  4.  0.  0.  0.  0.  0.  0. 12.  9.  0.  0.  0.  0.  0.  0.  7. 16.
  3.  0.  0.  0.  0.  0.  0. 14.  8.  0.  0.  0.  0.  0.  5. 15. 10.  0.
  0.  0.  0.  2. 15. 16.  2.  0.  0.  0.]
label :  5
predict :  (5, 1.0)
============================
dataset :  [ 0.  0.  7. 15.  6.  0.  0.  0.  0.  0.  2. 14. 15.  2.  0.  0.  0.  0.
  0.  5. 16.  6.  0.  0.  0.  0.  0.  5. 16.  9.  2.  0.  0.  5. 14. 16.
 15. 11.  4.  0.  0.  5.  7. 12. 11.  0.  0.  0.  0.  0.  4. 15.  1.  0.
  0.  0.  0.  0. 10. 11.  0.  0.  0.  0.]
label :  7
predict :  (7, 1.0)
============================
dataset :  [ 0.  0.  0.  5. 16.  3.  0.  0.  0.  0.  1. 15. 16.  4.  0.  0.  0.  0.
 10. 16. 16.  1.  0.  0.  0.  3. 16. 16. 15.  4.  2.  0.  0. 10. 16. 16.
 16. 16. 12.  0.  0.  1.  7. 14. 13.  6.  5.  0.  0.  0.  0. 11. 12.  0.
  0.  0.  0.  0.  0.  4. 10.  0.  0.  0.]
label :  4
predict :  (4, 1.0)
============================
dataset :  [ 0.  0.  0. 12.  4.  0.  0.  0.  0.  0.  6. 15.  2.  0.  0.  0.  0.  0.
 16.  5.  0.  4.  4.  0.  0.  4. 15.  2.  3. 15.  9.  0.  0.  2. 15. 16.
 16. 16.  4.  0.  0.  0.  2.  8. 16.  8.  0.  0.  0.  0.  0.  8. 15.  0.
  0.  0.  0.  0.  0. 11.  9.  0.  0.  0.]
label :  4
predict :  (4, 1.0)
============================
dataset :  [ 0.  0.  0. 13. 12.  0.  0.  0.  0.  0.  6. 16.  7.  0.  0.  0.  0.  0.
 15. 15.  1.  1.  4.  0.  0.  6. 16. 10.  9. 15. 14.  0.  0.  9. 16. 16.
 16. 16.  4.  0.  0.  2.  8. 12. 16.  9.  0.  0.  0.  0.  0. 13. 16.  0.
  0.  0.  0.  0.  0. 14. 15.  1.  0.  0.]
label :  4
predict :  (4, 1.0)
============================
dataset :  [ 0.  0.  1. 11.  3.  0.  0.  0.  0.  0.  9. 16.  0.  0.  0.  0.  0.  1.
 16.  5.  0.  1.  2.  0.  0.  6. 16.  2.  1. 13. 10.  0.  0.  7. 16.  9.
 15. 13.  0.  0.  0.  2.  9. 12. 16.  1.  0.  0.  0.  0.  0. 14.  9.  0.
  0.  0.  0.  0.  2. 16.  7.  0.  0.  0.]
label :  4
predict :  (4, 1.0)
============================
dataset :  [ 0.  0.  0.  8. 15.  2.  0.  0.  0.  0.  6. 16.  5.  0.  0.  0.  0.  0.
 12.  8.  0.  0.  0.  0.  0.  0. 13.  6.  0.  0.  0.  0.  0.  0. 12. 12.
 16. 14.  0.  0.  0.  0. 14. 15.  6.  8. 11.  0.  0.  3. 12. 14.  5. 10.
 13.  0.  0.  0.  0.  9. 16. 13.  5.  0.]
label :  6
predict :  (6, 1.0)
============================
dataset :  [ 0.  0.  1. 11. 16. 16. 10.  0.  0.  0. 13. 14.  8. 12. 11.  0.  0.  0.
  4.  0.  0. 13.  4.  0.  0.  0.  0.  0.  3. 15.  0.  0.  0.  0.  2. 15.
 16. 16.  9.  0.  0.  0.  3. 13. 16.  8.  1.  0.  0.  0.  0.  7. 10.  0.
  0.  0.  0.  0.  0. 13.  3.  0.  0.  0.]
label :  7
predict :  (7, 1.0)
============================
dataset :  [ 0.  0.  7. 16. 16.  3.  0.  0.  0.  7. 16.  9. 14.  7.  0.  0.  0. 10.
  9.  0. 14.  5.  0.  0.  0.  3.  3.  4. 16.  2.  0.  0.  0.  0.  0. 12.
 11.  0.  0.  0.  0.  0.  6. 16.  3.  0.  0.  0.  0.  0. 12. 15.  8.  8.
  3.  0.  0.  0. 10. 16. 16. 16.  9.  0.]
label :  2
predict :  (2, 1.0)
============================
dataset :  [ 0.  0.  1. 14.  8.  0.  0.  0.  0.  0.  8. 16.  4.  0.  0.  0.  0.  1.
 16.  9.  0.  1.  5.  0.  0.  8. 16.  5.  1. 12. 15.  0.  0. 10. 16. 12.
 11. 16.  6.  0.  0.  3. 14. 16. 16.  8.  0.  0.  0.  0.  0. 11. 16.  1.
  0.  0.  0.  0.  0. 13. 14.  0.  0.  0.]
label :  4
predict :  (4, 1.0)
============================
dataset :  [ 0.  0.  5.  8. 12. 16.  4.  0.  0.  3. 16. 11.  7.  1.  0.  0.  0.  3.
 14.  6.  4.  0.  0.  0.  0.  5. 16. 12. 14.  6.  0.  0.  0.  0.  2.  0.
  4. 12.  0.  0.  0.  0.  0.  0.  4. 10.  0.  0.  0.  0.  6.  8. 14.  7.
  0.  0.  0.  0.  7. 13.  7.  0.  0.  0.]
label :  5
predict :  (5, 1.0)
============================
dataset :  [ 0.  0. 10. 16. 16.  7.  0.  0.  0.  6. 15.  9. 14. 12.  0.  0.  0.  3.
  5.  0. 13.  8.  0.  0.  0.  0.  0. 10. 13.  0.  0.  0.  0.  0.  2. 16.
  4.  0.  0.  0.  0.  0. 12.  8.  0.  0.  0.  0.  0.  0. 16. 13. 11.  8.
  3.  0.  0.  0. 12. 16. 16. 16.  5.  0.]
label :  2
predict :  (2, 1.0)
============================
dataset :  [ 0.  0.  6. 15. 16.  3.  0.  0.  0.  3. 16. 12. 15.  8.  0.  0.  0.  0.
  4.  0. 14.  6.  0.  0.  0.  0.  0.  2. 16.  6.  2.  0.  0.  0.  4. 14.
 16. 16.  8.  0.  0.  0. 15. 16.  7.  0.  0.  0.  0.  0.  6. 16.  0.  0.
  0.  0.  0.  0.  7.  9.  0.  0.  0.  0.]
label :  7
predict :  (7, 1.0)
============================
dataset :  [ 0.  0.  1. 13.  7.  0.  0.  0.  0.  0.  7. 14.  2.  0.  0.  0.  0.  0.
 13.  5.  0.  0.  0.  0.  0.  0. 16.  3.  0.  0.  0.  0.  0.  3. 16. 10.
 12. 12.  3.  0.  0.  3. 16. 11.  5.  9. 12.  0.  0.  1. 13. 11.  4. 13.
 11.  0.  0.  0.  1. 12. 16. 11.  2.  0.]
label :  6
predict :  (6, 1.0)
============================
dataset :  [ 0.  0.  7. 13. 16. 11.  0.  0.  0.  3. 16.  5.  4. 14.  2.  0.  0.  8.
 11.  1.  4. 15.  2.  0.  0.  3. 12. 14. 16.  8.  0.  0.  0.  0.  8. 16.
 15.  1.  0.  0.  0.  1. 15.  5. 11. 12.  0.  0.  0.  3. 16.  5.  7. 16.
  1.  0.  0.  0.  5. 14. 16. 15.  2.  0.]
label :  8
predict :  (8, 1.0)
============================
dataset :  [ 0.  0.  0.  6. 16.  2.  0.  0.  0.  0.  2. 15. 15.  0.  0.  0.  0.  0.
 15. 16.  3.  2.  3.  0.  0.  7. 16.  7.  3. 15. 11.  0.  0.  7. 16. 14.
 14. 16.  5.  0.  0.  1.  7. 12. 16. 10.  0.  0.  0.  0.  0.  7. 16.  4.
  0.  0.  0.  0.  0. 10. 15.  0.  0.  0.]
label :  4
predict :  (4, 1.0)
============================
dataset :  [ 0.  0.  0.  2. 13.  1.  0.  0.  0.  0.  0.  9. 15.  2.  0.  0.  0.  0.
  4. 16. 16.  8.  0.  0.  0.  0. 12.  9. 14.  6.  0.  0.  0.  5. 14.  0.
 13.  7.  1.  0.  0.  9. 15. 12. 16. 16.  4.  0.  0.  2.  8.  9. 16. 10.
  1.  0.  0.  0.  0.  1. 13.  2.  0.  0.]
label :  4
predict :  (1, 1.0)
============================
dataset :  [ 0.  0.  0. 11.  4.  0.  0.  0.  0.  0.  1. 16.  4.  3.  0.  0.  0.  0.
 10.  9. 16.  4.  0.  0.  0.  2. 14.  5. 16.  2.  0.  0.  0.  8. 13.  7.
 16. 11.  2.  0.  0. 10. 16. 16. 16. 14.  1.  0.  0.  0.  0. 11. 13.  0.
  0.  0.  0.  0.  0. 11.  7.  0.  0.  0.]
label :  4
predict :  (4, 1.0)
============================
dataset :  [ 0.  0.  0.  4. 12.  0.  0.  0.  0.  0.  0. 12. 16.  8.  0.  0.  0.  0.
  4. 16. 15.  8.  0.  0.  0.  1. 15.  8. 14.  7.  0.  0.  0.  6. 16.  8.
 14. 14.  4.  0.  0. 10. 16. 16. 16. 13.  1.  0.  0.  0.  0.  2. 16.  4.
  0.  0.  0.  0.  0.  4. 13.  2.  0.  0.]
label :  4
predict :  (4, 1.0)
============================
dataset :  [ 0.  0.  1.  9. 14. 11.  1.  0.  0.  0. 10. 15.  9. 13.  5.  0.  0.  3.
 16.  7.  0.  0.  0.  0.  0.  5. 16. 16. 16. 10.  0.  0.  0.  7. 16. 11.
 10. 16.  5.  0.  0.  2. 16.  5.  0. 12.  8.  0.  0.  0. 10. 15. 13. 16.
  5.  0.  0.  0.  0.  9. 12.  7.  0.  0.]
label :  6
predict :  (6, 2.0548859729994952e-36)
============================
dataset :  [ 0.  0.  6. 12. 12. 15. 16.  6.  0.  2. 15. 16. 14. 16. 15.  3.  0.  3.
 16.  6.  6. 16.  6.  0.  0.  7. 15.  4. 14. 11.  0.  0.  0.  1.  2.  8.
 15.  3.  0.  0.  0.  0.  1. 16.  9.  0.  0.  0.  0.  0.  6. 16.  4.  0.
  0.  0.  0.  0.  8. 16.  3.  0.  0.  0.]
label :  7
predict :  (7, 1.0)
============================
dataset :  [ 0.  0.  7. 13.  2.  0.  0.  0.  0. 11. 15. 12. 13.  0.  0.  0.  0. 12.
  7.  0. 16.  4.  0.  0.  0.  4.  4.  0. 14.  8.  0.  0.  0.  0.  0.  0.
 14.  7.  0.  0.  0.  0.  0.  4. 16.  3.  0.  0.  0.  0. 12. 16. 16. 12.
  9.  0.  0.  0.  9. 12.  8. 10. 14.  0.]
label :  2
predict :  (1, 1.0)
============================
dataset :  [ 0.  0.  0.  5.  8.  0.  0.  0.  0.  0.  1. 15. 10.  5.  0.  0.  0.  0.
  9. 11. 10. 10.  0.  0.  0.  2. 15.  2. 14.  6.  0.  0.  0.  8. 13.  5.
 14. 13.  4.  0.  0. 11. 16. 16. 16. 14.  3.  0.  0.  0.  0.  3. 16.  0.
  0.  0.  0.  0.  0.  7. 10.  0.  0.  0.]
label :  4
predict :  (4, 1.0)
============================
dataset :  [ 0.  0.  9. 13. 14. 15. 13.  0.  0.  0. 16. 13. 12. 12.  5.  0.  0.  4.
 16.  0.  0.  0.  0.  0.  0.  8. 16. 16. 11.  1.  0.  0.  0.  1.  7.  8.
 16. 12.  0.  0.  0.  0.  0.  0.  9. 13.  0.  0.  0.  0. 12.  8. 12. 10.
  0.  0.  0.  0. 10. 16. 13.  3.  0.  0.]
label :  5
predict :  (5, 1.0)
============================
dataset :  [ 0.  0. 11. 11.  2.  0.  0.  0.  0.  3. 16. 16.  8.  0.  0.  0.  0.  6.
 12.  8.  8.  0.  0.  0.  0.  0.  5.  6. 12.  0.  0.  0.  0.  0.  0. 11.
  9.  0.  0.  0.  0.  0.  0. 15.  6.  3.  5.  0.  0.  0. 13. 16. 13. 15.
  9.  0.  0.  1. 12. 12. 12. 12.  1.  0.]
label :  2
predict :  (2, 1.0)
============================
dataset :  [ 0.  0.  4.  6. 11. 14.  6.  0.  0.  4. 16. 16. 12. 16.  7.  0.  0.  6.
 16.  2.  1. 16.  3.  0.  0.  5. 16.  0.  5. 14.  0.  0.  0.  0.  2.  0.
 11. 10.  0.  0.  0.  0.  0.  2. 15.  4.  0.  0.  0.  0.  0.  8. 16.  0.
  0.  0.  0.  0.  0.  7. 12.  0.  0.  0.]
label :  7
predict :  (7, 0.00678639434131262)
============================
dataset :  [ 0.  0.  0.  6. 14.  1.  0.  0.  0.  0.  1. 16. 10.  0.  0.  0.  0.  0.
 13. 14.  1.  0.  0.  0.  0.  2. 16. 12. 10.  3.  0.  0.  0.  5. 16. 15.
 14. 16.  1.  0.  0.  3. 16. 12.  0. 15.  8.  0.  0.  0. 11. 16.  9. 16.
  8.  0.  0.  0.  0. 11. 15. 11.  1.  0.]
label :  6
predict :  (6, 0.9999999999508196)
============================
dataset :  [ 0.  0.  3. 10. 14.  3.  0.  0.  0.  8. 16. 11. 10. 13.  0.  0.  0.  7.
 14.  0.  1. 15.  2.  0.  0.  2. 16.  9. 16. 16.  1.  0.  0.  0. 12. 16.
 15. 15.  2.  0.  0.  0. 12. 10.  0.  8.  8.  0.  0.  0.  9. 12.  4.  7.
 12.  0.  0.  0.  2. 11. 16. 16.  9.  0.]
label :  8
predict :  (8, 5.4115318540924516e-39)
============================
dataset :  [ 0.  0.  0.  5. 11.  0.  0.  0.  0.  0.  0. 10. 13.  0.  0.  0.  0.  0.
  0. 16. 16.  6.  0.  0.  0.  0.  9. 12. 16.  5.  0.  0.  0.  2. 16.  4.
 16.  7.  0.  0.  0.  9. 16. 14. 16. 16.  3.  0.  0.  3.  8. 11. 16.  8.
  1.  0.  0.  0.  0.  5. 13.  0.  0.  0.]
label :  4
predict :  (1, 1.0)
============================
dataset :  [ 0.  0.  1. 14.  3.  0.  0.  0.  0.  0.  8. 14.  0.  3.  0.  0.  0.  1.
 16.  4. 10. 12.  0.  0.  0.  7. 14.  2. 15.  5.  0.  0.  0. 13. 14. 11.
 16. 16.  9.  0.  0.  8. 16. 16. 14.  4.  0.  0.  0.  0.  0. 15.  9.  0.
  0.  0.  0.  0.  2. 16.  5.  0.  0.  0.]
label :  4
predict :  (4, 1.0)
============================
dataset :  [ 0.  0.  3. 16.  4.  0.  0.  0.  0.  0. 12. 13.  2.  5.  0.  0.  0.  2.
 16.  6. 10. 15.  1.  0.  0.  9. 15.  3. 16. 11.  7.  0.  0. 12. 16. 16.
 15. 11.  5.  0.  0.  3.  9. 16.  3.  0.  0.  0.  0.  0.  2. 16.  3.  0.
  0.  0.  0.  0.  6. 14.  0.  0.  0.  0.]
label :  4
predict :  (4, 1.0)
============================
dataset :  [ 0.  0.  0. 11.  8.  0.  0.  0.  0.  0.  2. 16.  5.  0.  0.  0.  0.  0.
 12. 10.  4. 10.  0.  0.  0.  6. 15.  2. 15.  8.  0.  0.  0. 10. 12.  4.
 16.  7.  6.  0.  0. 10. 16. 15. 16. 14.  6.  0.  0.  3.  8. 16.  9.  0.
  0.  0.  0.  0.  0. 14. 11.  0.  0.  0.]
label :  4
predict :  (4, 4.466030060468533e-22)
============================
dataset :  [ 0.  0.  2. 13. 16.  8.  0.  0.  0.  0. 11. 16.  6.  2.  0.  0.  0.  2.
 16.  8.  0.  0.  0.  0.  0.  5. 16.  9.  1.  0.  0.  0.  0.  5. 16. 16.
 13.  2.  0.  0.  0.  1. 16.  6.  8. 14.  0.  0.  0.  0. 11. 10.  1. 16.
  5.  0.  0.  0.  3. 15. 16. 16.  3.  0.]
label :  6
predict :  (8, 1.0)
============================
dataset :  [ 0.  0.  3. 15. 16. 16.  6.  0.  0.  0.  3. 14.  7. 15.  3.  0.  0.  0.
  0.  0.  4. 14.  0.  0.  0.  0.  2.  9. 15. 16. 14.  0.  0.  0.  7. 16.
 14.  6.  2.  0.  0.  0.  0.  8.  8.  0.  0.  0.  0.  0.  2. 16.  2.  0.
  0.  0.  0.  0.  5. 12.  0.  0.  0.  0.]
label :  7
predict :  (7, 1.0)
============================
dataset :  [ 0.  0.  7. 13. 14.  1.  0.  0.  0.  7. 15.  9. 13.  7.  0.  0.  0.  5.
 15.  3.  8.  8.  0.  0.  0.  0.  1.  0. 12.  5.  0.  0.  0.  0.  0.  1.
 14.  0.  0.  0.  0.  0.  0. 10.  6.  0.  0.  0.  0.  0.  2. 15.  5.  4.
  4.  0.  0.  0.  6. 16. 16. 13. 16.  6.]
label :  2
predict :  (2, 0.9999999998360962)
============================
dataset :  [ 0.  0.  0. 12. 10.  0.  0.  0.  0.  0.  4. 16.  5.  0.  0.  0.  0.  0.
 15.  7.  2. 14.  1.  0.  0.  6. 16.  2.  9. 16. 11.  0.  0.  9. 14.  9.
 16. 15.  6.  0.  0.  5. 16. 16. 16.  1.  0.  0.  0.  0.  2. 11. 13.  0.
  0.  0.  0.  0.  0. 12. 13.  0.  0.  0.]
label :  4
predict :  (4, 1.0)
============================
dataset :  [ 0.  0.  9. 16. 16. 13.  2.  0.  0.  2. 15.  2.  3.  3.  0.  0.  0.  7.
  9.  0.  1.  4.  0.  0.  0.  8. 12.  7. 13. 14.  7.  0.  0.  6. 16.  8.
  0.  5.  8.  0.  0.  1.  3.  0.  0.  9.  6.  0.  0.  0.  3.  4.  1. 15.
  0.  0.  0.  0.  7. 16. 12.  7.  0.  0.]
label :  5
predict :  (5, 1.0)
============================
dataset :  [ 0.  0. 10. 15.  2.  0.  0.  0.  0.  7. 16. 16.  6.  0.  0.  0.  0. 12.
 13. 12.  9.  0.  0.  0.  0.  8.  9. 13.  7.  0.  0.  0.  0.  0.  0. 16.
  5.  0.  0.  0.  0.  0.  6. 15.  1.  0.  0.  0.  0.  0. 16. 14.  4.  5.
  8.  3.  0.  0.  8. 16. 16. 16. 16.  9.]
label :  2
predict :  (2, 1.0)
============================
dataset :  [ 0.  0.  4. 15. 16. 12.  0.  0.  0.  0.  6.  9. 12. 10.  0.  0.  0.  0.
  0.  0. 10.  9.  0.  0.  0.  0.  2.  4. 15. 10.  4.  0.  0.  2. 15. 16.
 16. 15.  7.  0.  0.  0.  8. 13.  9.  0.  0.  0.  0.  0.  1. 16.  4.  0.
  0.  0.  0.  0.  6. 13.  0.  0.  0.  0.]
label :  7
predict :  (7, 1.0)
============================
dataset :  [ 0.  0.  0. 13. 15.  4.  0.  0.  0.  0. 11. 16.  9.  4.  0.  0.  0.  1.
 16. 14.  0.  0.  0.  0.  0.  5. 16.  7.  0.  0.  0.  0.  0.  5. 16. 16.
 14.  4.  0.  0.  0.  2. 15.  9.  7. 15.  5.  0.  0.  0. 11. 13.  4. 12.
 13.  0.  0.  0.  1. 13. 16. 16. 10.  0.]
label :  6
predict :  (6, 1.0)
============================
dataset :  [ 0.  0.  0.  7. 12. 13.  1.  0.  0.  0.  8. 11.  1. 10.  8.  0.  0.  0.
 12.  2.  1. 11.  7.  0.  0.  0. 10. 10. 14.  8.  0.  0.  0.  1.  7. 16.
  9.  0.  0.  0.  0.  7. 16.  7. 14.  3.  0.  0.  0.  0.  7. 13.  5. 14.
  0.  0.  0.  0.  0.  6. 15. 14.  2.  0.]
label :  8
predict :  (8, 1.0)
============================
dataset :  [ 0.  0.  3. 15.  6.  0.  0.  0.  0.  0.  9. 13.  1.  6.  9.  0.  0.  3.
 16.  3.  6. 15.  5.  0.  0.  7. 15.  1. 14.  9.  5.  0.  0. 10. 13.  9.
 16. 15.  7.  0.  0.  7. 16. 16. 11.  4.  0.  0.  0.  0.  3. 16.  5.  0.
  0.  0.  0.  0.  4. 16.  3.  0.  0.  0.]
label :  4
predict :  (4, 1.0)
============================
dataset :  [ 0.  0.  0.  0. 15.  7.  0.  0.  0.  0.  0. 10. 16.  6.  0.  0.  0.  0.
  8. 15. 14.  4.  0.  0.  0.  6. 15.  2. 15.  2.  1.  0.  0.  9. 16. 16.
 16. 16. 11.  0.  0.  5. 10. 12. 16.  8.  1.  0.  0.  0.  0.  1. 15.  0.
  0.  0.  0.  0.  0.  1. 15.  0.  0.  0.]
label :  4
predict :  (4, 1.0)
============================
dataset :  [ 0.  0.  0.  1. 15.  5.  0.  0.  0.  0.  0. 12. 16.  0.  0.  0.  0.  0.
  7. 16. 16.  3.  0.  0.  0.  5. 16.  8. 16.  8.  3.  0.  0. 11. 16. 12.
 16. 16. 12.  0.  0. 11. 16. 15. 16.  7.  2.  0.  0.  1.  4.  2. 16.  0.
  0.  0.  0.  0.  0.  2. 14.  0.  0.  0.]
label :  4
predict :  (4, 1.0)
============================
dataset :  [ 0.  0.  0.  0.  6. 15.  2.  0.  0.  0.  0.  5. 16. 16.  2.  0.  0.  0.
  4. 16. 12. 16.  0.  0.  0.  4. 15.  6.  7. 13.  0.  0.  0. 11. 15. 15.
 16. 16.  9.  0.  0.  9. 13. 12. 13. 14.  3.  0.  0.  0.  0.  0.  9.  8.
  0.  0.  0.  0.  0.  0.  8.  8.  0.  0.]
label :  4
predict :  (8, 2.6625155167556443e-25)
============================
dataset :  [ 0.  0.  4. 15.  4.  0.  0.  0.  0.  0.  9. 16.  2.  0.  0.  0.  0.  0.
 16. 10.  0.  0.  0.  0.  0.  6. 16.  3.  0.  0.  0.  0.  0. 10. 15. 11.
 16. 13.  4.  0.  0.  7. 16. 16. 11. 14. 14.  0.  0.  2. 16. 11.  5. 15.
 12.  0.  0.  0.  3. 16. 16. 14.  3.  0.]
label :  6
predict :  (6, 1.0)
============================
dataset :  [ 0.  0.  0.  3.  9. 16. 16.  2.  0.  0.  4. 16. 13. 11. 16.  1.  0.  0.
  3.  5.  0.  6. 13.  0.  0.  0.  0.  2.  7. 14.  9.  0.  0.  0.  4. 16.
 16. 15.  3.  0.  0.  0.  9.  8. 11. 12.  0.  0.  0.  0.  0.  0. 12.  4.
  0.  0.  0.  0.  0.  2. 15.  1.  0.  0.]
label :  7
predict :  (7, 1.0)
============================
dataset :  [ 0.  0.  3. 12. 16. 16.  3.  0.  0.  2. 16. 16. 11. 16.  4.  0.  0.  8.
 14.  2. 10. 16.  1.  0.  0.  5.  5.  3. 16.  4.  0.  0.  0.  0.  0. 11.
 12.  0.  0.  0.  0.  0.  3. 16.  5.  2.  3.  0.  0.  0.  3. 16. 12. 15.
  6.  0.  0.  0.  0. 15. 16.  8.  0.  0.]
label :  2
predict :  (2, 9.221663870547453e-26)
============================
dataset :  [ 0.  0.  0.  6. 15.  1.  0.  0.  0.  0.  3. 16.  9. 15.  3.  0.  0.  1.
 15.  7.  5. 15.  0.  0.  0.  9. 16.  4. 11. 14. 10.  0.  0.  9. 16. 16.
 16. 16.  9.  0.  0.  0.  2.  4. 16.  2.  0.  0.  0.  0.  0.  6. 14.  0.
  0.  0.  0.  0.  0.  7. 10.  0.  0.  0.]
label :  4
predict :  (4, 1.0)
============================
dataset :  [ 0.  0.  2. 16. 15. 15.  8.  0.  0.  0.  7. 16. 15. 12.  7.  0.  0.  3.
 15.  8.  1.  0.  0.  0.  0.  9. 15.  4.  4.  2.  0.  0.  0.  5. 16. 16.
 16. 15.  2.  0.  0.  0.  5.  6.  8. 16.  3.  0.  0.  0.  0.  1. 14. 10.
  0.  0.  0.  0.  2. 16. 13.  1.  0.  0.]
label :  5
predict :  (5, 3.211954652852293e-74)
============================
dataset :  [ 0.  2. 12. 16. 12.  0.  0.  0.  0.  7. 16. 13. 16.  3.  0.  0.  0.  0.
  3.  5. 16.  0.  0.  0.  0.  0.  3. 15.  7.  0.  0.  0.  0.  0. 11. 13.
  0.  0.  0.  0.  0.  6. 13.  1.  0.  0.  0.  0.  0.  6. 16. 11.  8. 11.
  5.  0.  0.  0. 15. 16. 16. 15.  3.  0.]
label :  2
predict :  (3, 4.718548944368838e-06)
============================
dataset :  [ 0.  0.  0.  7. 14. 16.  6.  0.  0.  0. 10. 16. 12. 15.  9.  0.  0.  0.
  8.  3.  2. 16.  7.  0.  0.  0.  1.  8. 13. 16. 14.  0.  0.  2. 13. 16.
 16. 12.  1.  0.  0.  6. 12.  6. 16.  3.  0.  0.  0.  0.  0.  5. 13.  0.
  0.  0.  0.  0.  0.  9.  6.  0.  0.  0.]
label :  7
predict :  (7, 1.0)
============================
dataset :  [ 0.  0.  0.  6. 11.  0.  0.  0.  0.  0.  0. 15. 10.  0.  0.  0.  0.  0.
  7. 15.  2.  0.  0.  0.  0.  0. 16.  6.  0.  0.  0.  0.  0.  3. 16.  7.
  5.  5.  0.  0.  0.  2. 16. 13.  9. 13. 11.  0.  0.  0.  8. 13.  7.  5.
 15.  3.  0.  0.  0.  5. 11. 13. 12.  2.]
label :  6
predict :  (6, 1.0)
============================
dataset :  [ 0.  0.  0.  8. 15.  9.  1.  0.  0.  0. 11. 14. 12. 15.  8.  0.  0.  0.
 15.  5.  6. 14.  2.  0.  0.  0. 14. 14. 15.  1.  0.  0.  0.  1. 13. 16.
  6.  0.  0.  0.  0.  6. 16.  9. 13.  0.  0.  0.  0.  2. 13. 15. 16.  4.
  0.  0.  0.  0.  1.  9. 15.  2.  0.  0.]
label :  8
predict :  (8, 1.0)
============================
dataset :  [ 0.  0.  0. 11.  5.  3. 11.  0.  0.  0.  7. 14.  2. 12.  9.  0.  0.  2.
 15.  6.  3. 16.  5.  0.  0.  7. 16.  8. 13. 16. 13.  0.  0.  7. 16. 16.
 16.  7.  1.  0.  0.  0.  4. 10. 13.  0.  0.  0.  0.  0.  0. 12.  6.  0.
  0.  0.  0.  0.  0. 12.  0.  0.  0.  0.]
label :  4
predict :  (7, 1.706924917219508e-13)
============================
dataset :  [ 0.  0.  0. 12. 12.  0.  0.  0.  0.  0.  5. 16.  4.  0.  0.  0.  0.  1.
 14. 11.  0.  0.  0.  0.  0.  6. 16.  3.  2.  0.  0.  0.  0. 13. 12.  8.
 12.  0.  0.  0.  0. 15. 16. 15. 16. 13.  4.  0.  0.  4.  9. 14. 16.  7.
  0.  0.  0.  0.  0. 11. 13.  0.  0.  0.]
label :  4
predict :  (6, 1.0)
============================
dataset :  [ 0.  0.  0.  8. 15.  5.  0.  0.  0.  0.  3. 16. 13.  1.  0.  0.  0.  0.
 12. 16.  2.  0.  0.  0.  0.  5. 16.  7.  9.  4.  0.  0.  0. 14. 16. 13.
 16. 14.  3.  0.  0.  8. 14. 16. 16. 14.  2.  0.  0.  0.  0.  9. 16.  3.
  0.  0.  0.  0.  0. 11. 14.  0.  0.  0.]
label :  4
predict :  (4, 1.0)
============================
dataset :  [ 0.  0.  0. 13.  9.  0.  0.  0.  0.  0.  6. 16.  2.  0.  0.  0.  0.  0.
 12.  9.  0.  2.  0.  0.  0.  7. 15.  1.  5. 15.  1.  0.  0. 14. 10.  4.
 11. 12.  3.  0.  2. 16. 16. 16. 16. 13.  2.  0.  0.  3.  4. 11. 14.  0.
  0.  0.  0.  0.  0. 15.  4.  0.  0.  0.]
label :  4
predict :  (4, 0.9999999899958635)
============================
dataset :  [ 0.  0.  6. 13.  0.  0.  0.  0.  0.  0. 15. 12.  0.  0.  0.  0.  0.  0.
 16.  6.  0.  0.  0.  0.  0.  3. 16. 14. 11.  5.  0.  0.  0.  5. 16. 12.
 11. 16.  6.  0.  0.  6. 16.  9.  2. 16.  9.  0.  0.  0. 13. 14.  8. 16.
  8.  0.  0.  0.  4. 15. 16. 13.  2.  0.]
label :  6
predict :  (5, 1.0)
============================
dataset :  [ 0.  0.  3. 15. 16. 15.  1.  0.  0.  0.  9. 11.  9. 16.  3.  0.  0.  0.
  1.  0.  3. 16.  3.  0.  0.  0.  0.  0.  9. 14.  0.  0.  0.  0.  4. 15.
 15. 16.  6.  0.  0.  0.  2. 12. 15.  7.  1.  0.  0.  0.  0. 13.  8.  0.
  0.  0.  0.  0.  4. 14.  1.  0.  0.  0.]
label :  7
predict :  (7, 1.0)
============================
dataset :  [ 0.  3. 15. 16. 14.  1.  0.  0.  0.  2. 12. 13. 16.  4.  0.  0.  0.  0.
  0.  6. 16.  3.  0.  0.  0.  0.  1. 15. 10.  0.  0.  0.  0.  0.  6. 16.
  4.  0.  0.  0.  0.  2. 15. 10.  0.  0.  0.  0.  0.  4. 16. 11.  8. 11.
  3.  0.  0.  3. 16. 16. 16. 12.  3.  0.]
label :  2
predict :  (2, 0.9999997915256996)
============================
dataset :  [ 0.  0.  0.  8. 15.  2.  0.  0.  0.  0.  2. 16. 10.  0.  0.  0.  0.  0.
 14. 13.  6. 11.  0.  0.  0.  6. 16.  3. 13. 13.  2.  0.  0. 14. 16.  8.
 15. 16. 10.  0.  0. 12. 16. 16. 16. 11.  1.  0.  0.  0.  1.  6. 16.  3.
  0.  0.  0.  0.  0. 10. 14.  0.  0.  0.]
label :  4
predict :  (4, 1.0)
============================
dataset :  [ 0.  2. 13. 13. 11.  9.  0.  0.  0. 10. 16. 16. 16. 15. 10.  0.  0. 11.
 16.  9.  0.  0.  0.  0.  0.  3. 15. 16.  8.  0.  0.  0.  0.  0.  2. 11.
 14.  0.  0.  0.  0.  0.  0.  8. 16.  0.  0.  0.  0.  0.  1. 11. 11.  0.
  0.  0.  0.  1. 16. 15.  4.  0.  0.  0.]
label :  5
predict :  (5, 1.0)
============================
dataset :  [ 0.  0.  9. 16. 16. 13.  1.  0.  0.  0. 12. 13. 14. 16.  7.  0.  0.  0.
  0.  0.  6. 16.  4.  0.  0.  0.  0.  0. 13. 14.  1.  0.  0.  0.  1. 10.
 16.  6.  0.  0.  0.  0.  7. 16.  8.  0.  0.  0.  0.  2. 15. 16. 12.  7.
  0.  0.  0.  0.  9. 14. 16. 16.  2.  0.]
label :  2
predict :  (2, 1.0)
============================
dataset :  [ 0.  0.  6. 16. 15.  2.  0.  0.  0.  0.  7. 13. 16.  4.  0.  0.  0.  0.
  0.  1. 16.  3.  0.  0.  0.  0.  1. 10. 16.  6.  1.  0.  0.  0.  9. 16.
 16. 16.  8.  0.  0.  0.  1. 16.  8.  4.  0.  0.  0.  0.  5. 13.  0.  0.
  0.  0.  0.  0. 11.  7.  0.  0.  0.  0.]
label :  7
predict :  (7, 1.0)
============================
dataset :  [ 0.  0.  0.  7. 13.  2.  0.  0.  0.  0.  0. 14. 14.  2.  0.  0.  0.  0.
  5. 16.  4.  0.  0.  0.  0.  1. 11. 16.  4.  0.  0.  0.  0.  5. 16. 16.
 15. 12.  0.  0.  0.  0.  9. 16.  1. 13.  7.  0.  0.  0.  4. 16.  6. 15.
  5.  0.  0.  0.  0.  6. 14. 14.  1.  0.]
label :  6
predict :  (6, 1.0)
============================
dataset :  [ 0.  0.  1.  8. 14. 14.  2.  0.  0.  1. 13. 16. 16. 16.  5.  0.  0.  7.
 16. 10. 10. 16.  4.  0.  0.  3. 16. 14. 15. 12.  0.  0.  0.  0.  3. 12.
 16. 10.  0.  0.  0.  0.  0.  9. 16. 16.  3.  0.  0.  0.  0. 15. 16. 16.
  4.  0.  0.  0.  0. 11. 16. 12.  2.  0.]
label :  8
predict :  (8, 2.330290719164281e-28)
============================
dataset :  [ 0.  0.  0.  4. 15.  6.  0.  0.  0.  0.  0. 13. 13.  1.  0.  0.  0.  0.
  7. 16.  2.  0.  0.  0.  0.  4. 15.  8.  0.  5.  0.  0.  0. 11. 14.  1.
  6. 16.  5.  0.  1. 16. 14. 12. 16. 16.  3.  0.  0. 10. 12. 10. 16. 10.
  0.  0.  0.  0.  0.  6. 16.  2.  0.  0.]
label :  4
predict :  (4, 1.211645926736686e-21)
============================
dataset :  [ 0.  0. 14. 12. 12. 13.  3.  0.  0.  0. 16.  8.  8.  6.  1.  0.  0.  0.
 14.  7.  5.  0.  0.  0.  0.  0. 15. 15. 16.  2.  0.  0.  0.  0. 13.  3.
  6.  8.  0.  0.  0.  0.  0.  0.  3. 13.  0.  0.  0.  0.  5.  4.  8. 12.
  1.  0.  0.  1. 15. 15. 11.  3.  0.  0.]
label :  5
predict :  (5, 1.0)
============================
dataset :  [ 0.  1. 12. 13. 13.  0.  0.  0.  0.  4. 11.  6.  3.  0.  0.  0.  0.  7.
 11.  8.  6.  1.  0.  0.  0.  5. 15. 12. 13. 12.  0.  0.  0.  0.  0.  0.
  0. 13.  4.  0.  0.  0.  0.  0.  0.  8.  8.  0.  0.  2. 10.  8.  7. 15.
  3.  0.  0.  1. 13. 16. 12.  5.  0.  0.]
label :  5
predict :  (5, 1.0)
============================
dataset :  [ 0.  0.  9. 12. 14.  2.  0.  0.  0.  0. 12.  6.  4.  0.  0.  0.  0.  0.
 12.  1.  3.  0.  0.  0.  0.  0.  9. 16. 16. 12.  0.  0.  0.  0.  4.  4.
  0. 12.  6.  0.  0.  0.  0.  0.  0.  4. 12.  0.  0.  0.  9.  7.  4. 10.
 11.  0.  0.  0.  9. 14. 16. 14.  5.  0.]
label :  5
predict :  (5, 1.0)
============================
dataset :  [ 0.  3. 12. 12. 14.  4.  0.  0.  0.  1. 13.  4.  4.  0.  0.  0.  0.  4.
 14.  4.  3.  0.  0.  0.  0.  5. 13. 12. 14. 10.  0.  0.  0.  0.  0.  0.
  0. 11.  6.  0.  0.  0.  0.  0.  0.  4.  8.  0.  0.  0.  6.  2.  0.  8.
  8.  0.  0.  2. 13. 16. 16. 16.  2.  0.]
label :  5
predict :  (5, 1.0)
============================
dataset :  [ 0.  1. 10. 16. 15.  1.  0.  0.  0.  3. 15. 10. 16.  4.  0.  0.  0.  0.
  1. 11. 15.  0.  0.  0.  0.  0. 12. 16. 15.  3.  0.  0.  0.  0.  0.  1.
 11. 15.  1.  0.  0.  8.  3.  0.  3. 16.  7.  0.  0. 13. 15.  6.  8. 16.
  6.  0.  0.  0. 12. 16. 16.  7.  0.  0.]
label :  3
predict :  (3, 1.0)
============================
dataset :  [ 0.  0.  2. 12.  9.  0.  0.  0.  0.  0. 12. 10.  1.  0.  0.  0.  0.  4.
 14.  0.  0.  0.  0.  0.  0.  8.  9.  0.  0.  0.  0.  0.  0.  8.  9.  5.
 11.  8.  0.  0.  0.  4. 16. 14.  6. 12.  5.  0.  0.  0. 13.  7.  0. 10.
  8.  0.  0.  0.  3. 14. 16. 16.  5.  0.]
label :  6
predict :  (8, 0.656909060907352)
============================
dataset :  [ 0.  0.  0.  3. 16.  2.  0.  0.  0.  0.  0. 10. 13.  3.  8.  0.  0.  0.
  1. 16.  5.  9. 16.  0.  0.  2. 12. 14.  5. 15.  9.  0.  0. 12. 16. 16.
 16. 16.  7.  0.  0.  5.  5.  6. 14. 16.  0.  0.  0.  0.  0.  1. 13. 12.
  0.  0.  0.  0.  0.  3. 16.  4.  0.  0.]
label :  4
predict :  (4, 1.0)
============================
dataset :  [ 0.  0. 10. 10. 12.  7.  0.  0.  0.  0. 15. 13.  5. 12.  5.  0.  0.  4.
 13.  4.  0.  2.  8.  0.  0.  8.  4.  0.  0.  3.  8.  0.  0.  8.  4.  0.
  0.  7.  5.  0.  0.  6.  6.  0.  0. 11.  2.  0.  0.  1. 13.  3.  3. 12.
  0.  0.  0.  0.  7. 15. 16.  7.  0.  0.]
label :  0
predict :  (5, 8.688355186044931e-27)
============================
dataset :  [ 0.  0.  6. 14. 13.  3.  0.  0.  0.  0. 12.  2.  3. 14.  0.  0.  0.  0.
  0.  0.  8. 13.  0.  0.  0.  0.  0. 12. 16.  3.  0.  0.  0.  0.  0.  0.
  8. 13.  1.  0.  0.  1.  7.  0.  0.  7. 11.  0.  0.  3. 13.  2.  0.  7.
 13.  0.  0.  0.  5. 14. 14. 15.  6.  0.]
label :  3
predict :  (3, 1.0)
============================
dataset :  [ 0.  0.  1. 14.  2.  0.  0.  0.  0.  0.  0. 16.  5.  0.  0.  0.  0.  0.
  0. 14. 10.  0.  0.  0.  0.  0.  0. 11. 16.  1.  0.  0.  0.  0.  0.  3.
 14.  6.  0.  0.  0.  0.  0.  0.  8. 12.  0.  0.  0.  0. 10. 14. 13. 16.
  8.  3.  0.  0.  2. 11. 12. 15. 16. 15.]
label :  1
predict :  (1, 1.0)
============================
dataset :  [ 0.  0.  9. 12. 12. 12.  6.  0.  0.  1. 14.  6.  4.  4.  2.  0.  0.  4.
 15. 12.  9.  1.  0.  0.  0.  4. 15.  8. 11. 11.  0.  0.  0.  0.  1.  0.
  0. 14.  4.  0.  0.  0.  0.  0.  0. 10.  8.  0.  0.  0. 10.  1.  0.  8.
  8.  0.  0.  0.  9. 16. 16. 15.  4.  0.]
label :  5
predict :  (5, 1.0)
============================
dataset :  [ 0.  0.  6. 12. 13.  2.  0.  0.  0.  3. 16.  6.  1. 15.  0.  0.  0.  5.
 16. 13. 12. 16.  2.  0.  0.  2. 13. 16. 12. 15.  4.  0.  0.  0.  0.  0.
  0.  8.  8.  0.  0.  0.  1.  0.  0.  8.  8.  0.  0.  3. 16.  2.  0. 10.
  7.  0.  0.  0.  5. 11. 16. 13.  1.  0.]
label :  9
predict :  (9, 0.9999999999999993)
============================
dataset :  [ 0.  0.  5. 12. 12.  9.  3.  0.  0.  0.  8. 16. 16. 16.  4.  0.  0.  0.
  9. 16. 16. 14.  1.  0.  0.  0. 11. 16. 16. 12.  0.  0.  0.  0. 12. 16.
 16. 12.  0.  0.  0.  0. 11. 16. 16. 12.  0.  0.  0.  0.  4. 16. 16. 12.
  0.  0.  0.  0.  6. 12. 12.  6.  0.  0.]
label :  1
predict :  (8, 3.5040506479232606e-43)
============================
dataset :  [ 0.  0.  6. 10.  8.  3.  0.  0.  0.  0.  6. 16. 16.  9.  0.  0.  0.  0.
  9. 16. 16.  6.  0.  0.  0.  0.  7. 16. 16. 10.  0.  0.  0.  0. 11. 16.
 16.  8.  0.  0.  0.  0.  7. 16. 16.  9.  0.  0.  0.  0. 10. 16. 16.  6.
  0.  0.  0.  0.  4.  9. 12. 11.  2.  0.]
label :  1
predict :  (1, 1.0)
============================
dataset :  [ 0.  0.  3. 13. 10.  1.  0.  0.  0.  0.  3. 16. 16.  4.  0.  0.  0.  0.
  1. 16. 16.  2.  0.  0.  0.  0.  6. 16. 16.  1.  0.  0.  0.  0.  4. 16.
 16.  1.  0.  0.  0.  0.  4. 16. 16.  3.  0.  0.  0.  0.  7. 16. 16.  0.
  0.  0.  0.  0.  2. 14. 16.  5.  0.  0.]
label :  1
predict :  (1, 0.9999999999993276)
============================
dataset :  [ 0.  0. 12. 14.  6.  0.  0.  0.  0.  2. 16.  7. 13. 10.  0.  0.  0.  0.
 16.  2.  1. 13.  4.  0.  0.  0.  9. 13.  8. 16.  2.  0.  0.  0.  6. 16.
 16. 13.  0.  0.  0.  0.  0.  2.  3. 16.  0.  0.  0.  0.  1.  6. 13. 10.
  0.  0.  0.  0. 13.  9.  8.  2.  0.  0.]
label :  9
predict :  (9, 0.9999999999855025)
============================
dataset :  [ 0.  0.  0.  3. 16.  5.  0.  0.  0.  0.  3. 14. 10.  0.  9. 11.  0.  1.
 13. 11.  0.  2. 15.  8.  0.  7. 16.  9. 11. 16. 15.  1.  0.  6. 15. 13.
 12. 16.  9.  0.  0.  0.  0.  0.  8. 15.  2.  0.  0.  0.  0.  1. 15.  7.
  0.  0.  0.  0.  0.  5. 15.  2.  0.  0.]
label :  4
predict :  (4, 1.0)
============================
dataset :  [ 0.  0.  9. 16.  6.  0.  0.  0.  0.  3. 16.  1. 16. 10.  8.  0.  0.  0.
 15.  6. 16.  8.  0.  0.  0.  0.  3. 16. 11.  0.  0.  0.  0.  0.  1. 14.
 12.  0.  0.  0.  0.  0.  6.  9. 11.  2.  0.  0.  0.  0. 12.  1. 13.  0.
  0.  0.  0.  0. 12. 14.  3.  0.  0.  0.]
label :  8
predict :  (9, 1.1632025873807807e-08)
============================
dataset :  [ 0.  1. 12. 16. 16.  9.  0.  0.  0. 11. 15.  9.  7. 16.  3.  0.  0. 13.
  3.  1. 10. 15.  1.  0.  0.  0.  0. 11. 16.  8.  0.  0.  0.  0.  0.  5.
 15. 16.  5.  0.  0.  0.  0.  0.  0. 10. 13.  0.  0.  0.  7.  4.  8. 15.
  9.  0.  0.  0. 13. 16. 16. 12.  1.  0.]
label :  3
predict :  (3, 1.0)
============================
dataset :  [ 0.  1. 12. 15. 10.  2.  0.  0.  0.  4. 14.  1.  6. 12.  2.  0.  0.  7.
 15.  0.  1. 14.  4.  0.  0.  3. 15. 12. 15. 10.  0.  0.  0.  0.  3. 15.
  1.  0.  0.  0.  0.  0.  0.  3. 13.  1.  0.  0.  0.  0.  0.  0. 10.  6.
  0.  0.  0.  0. 11. 12. 13.  4.  0.  0.]
label :  9
predict :  (9, 1.0)
============================
dataset :  [ 0.  1. 10. 14. 13.  4.  0.  0.  0. 12. 11.  5.  8. 14.  0.  0.  0.  8.
  3.  2. 12.  8.  0.  0.  0.  0.  3. 15. 15.  4.  0.  0.  0.  0.  1.  4.
  7. 14.  5.  0.  0.  0.  0.  0.  0.  7. 12.  0.  0.  0.  0.  0.  1. 11.
 11.  0.  0.  0. 12. 16. 16.  9.  1.  0.]
label :  3
predict :  (3, 1.0)
============================
dataset :  [ 0.  0.  1. 11. 14.  5.  0.  0.  0.  0.  0. 15. 16. 11.  0.  0.  0.  0.
  5. 16. 16.  8.  0.  0.  0.  0.  5. 16. 16.  5.  0.  0.  0.  0.  4. 16.
 16.  3.  0.  0.  0.  0.  9. 16. 16.  2.  0.  0.  0.  0.  8. 16. 14.  0.
  0.  0.  0.  0.  2. 13. 16.  9.  0.  0.]
label :  1
predict :  (1, 5.637280219969882e-23)
============================
dataset :  [ 0.  1. 13. 16. 16. 12.  1.  0.  0. 12. 15.  6. 12. 16.  3.  0.  0. 13.
 10. 10. 16.  9.  0.  0.  0.  0.  5. 16. 15.  4.  0.  0.  0.  0.  0.  2.
 10. 14.  1.  0.  0.  0.  0.  0.  1. 16.  7.  0.  0.  0.  3.  1.  5. 16.
  9.  0.  0.  1. 14. 16. 16. 11.  1.  0.]
label :  3
predict :  (3, 0.7341667685556206)
============================
dataset :  [ 0.  2. 15. 15.  3.  0.  0.  0.  0. 11. 15. 11. 12.  0.  0.  0.  0.  8.
 10.  0. 16.  0.  0.  0.  0.  1.  3.  2. 16.  0.  0.  0.  0.  0.  0.  4.
 12.  0.  0.  0.  0.  0.  1. 12.  9.  0.  0.  0.  0.  0. 14. 16. 13. 13.
 15.  3.  0.  2. 13. 14. 12. 12.  8.  1.]
label :  2
predict :  (2, 1.0)
============================
dataset :  [ 0.  1. 14. 16. 16. 11.  2.  0.  0.  0. 14.  9.  2. 10. 11.  0.  0.  0.
  5. 16.  5. 14.  5.  0.  0.  0.  0. 12. 16. 16.  5.  0.  0.  0.  7. 14.
 14.  8.  0.  0.  0.  1. 14.  3.  6. 11.  0.  0.  0.  4.  8.  0.  8. 11.
  0.  0.  0.  2. 13. 12. 15.  2.  0.  0.]
label :  8
predict :  (8, 1.0)
============================
dataset :  [ 0.  1.  9. 15. 16.  6.  0.  0.  0. 13. 15. 10. 16. 11.  0.  0.  0.  5.
  3.  4. 16.  7.  0.  0.  0.  0.  0.  8. 16.  7.  0.  0.  0.  0.  0.  1.
 13. 15.  5.  0.  0.  0.  0.  0.  2. 13. 11.  0.  0.  0. 12.  5.  3. 13.
 14.  0.  0.  0. 10. 16. 16. 14.  5.  0.]
label :  3
predict :  (3, 1.0)
============================
dataset :  [ 0.  0.  7. 14. 16. 13.  1.  0.  0.  9. 15.  8. 10. 16.  7.  0.  0.  5.
  1.  0. 14. 14.  1.  0.  0.  0.  0.  4. 16. 12.  0.  0.  0.  0.  0.  2.
 16. 13.  0.  0.  0.  0.  0.  0.  6. 16.  7.  0.  0.  0.  9.  7.  6. 16.
  9.  0.  0.  0.  5. 15. 16. 11.  3.  0.]
label :  3
predict :  (3, 1.0)
============================
dataset :  [ 0.  1.  8. 10. 14. 10.  0.  0.  0. 12. 10.  6.  6. 16.  2.  0.  0.  3.
  0.  0.  7. 14.  1.  0.  0.  0.  0.  0.  9. 11.  0.  0.  0.  0.  0.  0.
  2. 16.  1.  0.  0.  0.  0.  0.  0.  8. 12.  0.  0.  0.  4.  0.  2. 11.
  9.  0.  0.  0. 12. 16. 14. 12.  4.  0.]
label :  3
predict :  (2, 0.8728220165136221)
============================
dataset :  [ 0.  3. 16. 13. 15. 16. 11.  0.  0.  5. 16. 14. 12.  8. 10.  0.  0.  2.
 16. 12.  0.  0.  0.  0.  0.  0.  7. 16. 12.  0.  0.  0.  0.  0.  0.  7.
 16.  8.  0.  0.  0.  0.  0.  0. 13. 11.  0.  0.  0.  3.  6.  8. 16.  7.
  0.  0.  0.  2. 15. 15.  9.  0.  0.  0.]
label :  5
predict :  (5, 1.0)
============================
dataset :  [ 0.  0. 12. 16. 16. 12.  0.  0.  0.  0.  6.  4. 10. 13.  1.  0.  0.  0.
  0.  0. 13.  9.  0.  0.  0.  0.  5.  9. 16. 16. 12.  0.  0.  3. 16. 16.
 11.  3.  0.  0.  0.  0.  7. 13.  0.  0.  0.  0.  0.  0. 11.  8.  0.  0.
  0.  0.  0.  0. 16.  3.  0.  0.  0.  0.]
label :  7
predict :  (1, 7.12112428074919e-05)
============================
dataset :  [ 0.  0.  5. 12. 16. 10.  0.  0.  0.  6. 16. 13.  3. 15.  1.  0.  0. 11.
  8.  5.  5. 10.  0.  0.  0.  4. 11.  2. 12.  2.  0.  0.  0.  0.  6. 16.
  6.  0.  0.  0.  0.  0.  2. 15.  8.  0.  0.  0.  0.  0.  8. 13.  8.  0.
  0.  0.  0.  0.  5. 15.  4.  0.  0.  0.]
label :  8
predict :  (8, 0.9735439965555919)
============================
dataset :  [ 0.  2. 13. 16. 16. 15.  4.  0.  0.  7. 12.  8.  8. 16. 12.  0.  0.  0.
  0.  0.  8. 16.  7.  0.  0.  0.  0.  0. 14. 10.  0.  0.  0.  0.  0.  0.
 12. 15.  3.  0.  0.  0.  0.  0.  2. 16. 11.  0.  0.  0.  4.  4.  7. 16.
 10.  0.  0.  2. 15. 16. 16. 12.  1.  0.]
label :  3
predict :  (3, 1.0)
============================
dataset :  [ 0.  0.  9. 16. 16.  7.  0.  0.  0.  2. 16. 11.  4. 15.  2.  0.  0.  4.
 16.  2.  2. 16.  6.  0.  0.  0. 13. 11. 13. 16. 10.  0.  0.  0.  1. 10.
 13. 16.  6.  0.  0.  0.  0.  0.  3. 16.  7.  0.  0.  0.  0.  2. 13. 14.
  1.  0.  0.  0. 11. 15. 15.  6.  0.  0.]
label :  9
predict :  (9, 2.736492225391642e-07)
============================
dataset :  [ 0.  3. 12. 15. 16. 16.  3.  0.  0.  6. 16.  9.  9. 16.  6.  0.  0.  0.
  3.  0. 11. 15.  1.  0.  0.  0.  0.  4. 16.  7.  0.  0.  0.  0.  0.  7.
 16.  4.  0.  0.  0.  0.  0.  0. 13. 11.  0.  0.  0.  0.  4.  5. 15. 14.
  0.  0.  0.  3. 16. 16. 15.  6.  0.  0.]
label :  3
predict :  (3, 0.9999979349866254)
============================
dataset :  [ 0.  0.  3. 12. 12.  3.  0.  0.  0.  0.  4. 16. 16.  4.  0.  0.  0.  0.
  5. 16. 16.  5.  0.  0.  0.  0. 11. 16. 15.  0.  0.  0.  0.  0. 12. 16.
 14.  0.  0.  0.  0.  0. 13. 16.  9.  0.  0.  0.  0.  0.  7. 16. 10.  1.
  0.  0.  0.  0.  5. 13. 14.  4.  0.  0.]
label :  1
predict :  (1, 3.2958958717692444e-32)
============================
dataset :  [ 0.  4. 13. 16. 16. 15.  3.  0.  0. 10. 12.  7.  8. 16.  8.  0.  0.  0.
  0.  1. 12. 15.  2.  0.  0.  0.  0.  4. 16. 10.  0.  0.  0.  0.  0.  8.
 16.  4.  0.  0.  0.  0.  0.  1. 16. 10.  0.  0.  0.  5.  7.  4. 15. 13.
  0.  0.  0.  5. 16. 16. 16.  7.  0.  0.]
label :  3
predict :  (2, 1.0)
============================
dataset :  [ 0.  2. 13. 16. 10.  0.  0.  0.  0.  6. 13. 10. 16.  0.  0.  0.  0.  0.
  0.  8. 14.  0.  0.  0.  0.  0.  1. 14.  9.  0.  0.  0.  0.  0.  9. 14.
  1.  0.  0.  0.  0.  1. 14.  7.  0.  0.  1.  0.  0.  4. 16.  5.  7. 12.
 14.  0.  0.  3. 15. 16. 16. 10.  1.  0.]
label :  2
predict :  (2, 1.9686593799695564e-13)
============================
dataset :  [ 0.  0. 13. 16. 16. 15.  2.  0.  0.  0. 14. 13. 11. 16.  2.  0.  0.  0.
 11. 13. 15.  6.  0.  0.  0.  0.  5. 16. 10.  0.  0.  0.  0.  0. 10. 14.
 15.  0.  0.  0.  0.  1. 14.  3. 15.  7.  0.  0.  0.  6. 11.  0. 15.  6.
  0.  0.  0.  1. 13. 16. 15.  3.  0.  0.]
label :  8
predict :  (8, 0.9999962890058377)
============================
dataset :  [ 0.  4. 14. 16. 16. 15.  2.  0.  0. 12. 11.  2.  4. 16.  5.  0.  0.  2.
  0.  1. 11. 12.  0.  0.  0.  0.  0. 15. 16.  3.  0.  0.  0.  0.  0.  4.
 15. 10.  0.  0.  0.  0.  0.  0.  1. 14. 10.  0.  0.  1.  7.  0.  3. 14.
  8.  0.  0.  4. 15. 16. 16. 11.  0.  0.]
label :  3
predict :  (3, 1.0)
============================
dataset :  [ 0.  0.  9. 16. 16. 12.  2.  0.  0.  3. 13.  5.  4. 14.  5.  0.  0.  0.
  0.  0.  7. 15.  2.  0.  0.  0.  0.  5. 16. 11.  0.  0.  0.  0.  0.  0.
  8. 16.  7.  0.  0.  0.  0.  0.  0. 13.  8.  0.  0.  0.  4.  5.  5. 15.
  4.  0.  0.  0. 12. 16. 15.  5.  0.  0.]
label :  3
predict :  (3, 1.0)
============================
dataset :  [ 0.  1. 13. 16. 16.  8.  0.  0.  0. 11. 13.  4. 13.  7.  0.  0.  0.  7.
  1.  7. 16.  1.  0.  0.  0.  0.  5. 16. 15.  9.  0.  0.  0.  0.  3.  6.
  8. 15.  8.  0.  0.  0.  6.  1.  0.  7. 13.  0.  0.  4. 16.  5.  2. 13.
 10.  0.  0.  1. 12. 16. 16. 11.  1.  0.]
label :  3
predict :  (2, 1.0)
============================
dataset :  [ 0.  0.  8. 16. 16.  9.  0.  0.  0.  1. 16. 15. 11.  8.  0.  0.  0.  1.
 14. 10.  0.  0.  0.  0.  0.  7. 16. 10.  6.  0.  0.  0.  0.  2. 12. 16.
 16. 10.  0.  0.  0.  0.  0.  1.  7. 15.  0.  0.  0.  0. 11.  5. 13. 13.
  0.  0.  0.  0. 11. 16. 16.  6.  0.  0.]
label :  5
predict :  (5, 3.206555049315398e-18)
============================
dataset :  [ 0.  0.  3. 11. 16. 15.  2.  0.  0.  4. 16. 10.  4. 16.  4.  0.  0.  7.
  6.  0.  5. 16.  1.  0.  0.  0.  0.  0. 10. 12.  0.  0.  0.  0.  0.  9.
 16. 16. 10.  0.  0.  0.  0.  6. 15.  6.  1.  0.  0.  0.  0. 13.  9.  0.
  0.  0.  0.  0.  1. 15.  2.  0.  0.  0.]
label :  7
predict :  (7, 1.0)
============================
dataset :  [ 0.  0.  6. 14. 16.  6.  0.  0.  0.  6. 16. 16.  8. 15.  0.  0.  0.  7.
 14. 14. 12. 14.  0.  0.  0.  0. 13. 10. 16.  6.  0.  0.  0.  0.  4. 16.
 10.  0.  0.  0.  0.  0. 11. 13. 16.  2.  0.  0.  0.  0. 15.  5. 15.  4.
  0.  0.  0.  0.  8. 16. 15.  1.  0.  0.]
label :  8
predict :  (8, 0.9999999968914535)
============================
dataset :  [ 0.  0. 10. 16. 16. 10.  1.  0.  0.  4. 16. 11. 11. 16.  3.  0.  0.  1.
  9.  1. 10. 15.  1.  0.  0.  0.  0.  5. 16. 10.  0.  0.  0.  0.  0.  0.
  7. 15. 10.  0.  0.  0.  0.  0.  0.  7. 16.  0.  0.  2. 12.  7.  4. 14.
 15.  1.  0.  0. 11. 16. 16. 15.  4.  0.]
label :  3
predict :  (9, 0.999977724084615)
============================
dataset :  [ 0.  0.  8. 16.  6.  3.  0.  0.  0.  2. 13.  5. 10. 14.  0.  0.  0.  4.
 14.  1.  9. 16.  0.  0.  0.  0. 12. 13.  8. 13.  0.  0.  0.  0.  0.  3.
  0. 11.  2.  0.  0.  0.  0.  0.  0. 12.  2.  0.  0.  0.  4.  1.  0. 14.
  1.  0.  0.  0.  6. 15. 16. 10.  0.  0.]
label :  9
predict :  (9, 1.0)
============================
dataset :  [ 0.  0. 13. 16. 16.  9.  0.  0.  0.  2. 16.  7.  7. 16.  0.  0.  0.  0.
  4.  0. 11. 10.  0.  0.  0.  0.  1. 13. 14.  3.  0.  0.  0.  0.  0.  7.
 15. 11.  1.  0.  0.  0.  0.  1.  2. 13. 10.  0.  0.  0.  8.  9.  1. 12.
 11.  0.  0.  0. 11. 16. 16. 15.  1.  0.]
label :  3
predict :  (3, 1.0)
============================
dataset :  [ 0.  0.  2. 16. 10.  0.  0.  0.  0.  0.  3. 16. 16.  1.  0.  0.  0.  0.
  5. 16. 14.  0.  0.  0.  0.  0.  3. 16. 13.  0.  0.  0.  0.  0.  1. 16.
 15.  0.  0.  0.  0.  0.  1. 16. 16.  0.  0.  0.  0.  0.  2. 16. 15.  2.
  0.  0.  0.  0.  0. 15. 16. 11.  0.  0.]
label :  1
predict :  (2, 3.1840040930605816e-12)
============================
dataset :  [ 0.  0.  9. 16. 16.  7.  0.  0.  0. 13. 15.  9. 12. 15.  0.  0.  0.  5.
  4.  0. 13. 13.  0.  0.  0.  0.  0. 11. 16.  5.  0.  0.  0.  0.  0. 11.
 16. 10.  3.  0.  0.  0.  0.  0.  4. 12. 13.  0.  0.  0.  7.  1.  1. 12.
 14.  0.  0.  0.  9. 16. 16. 14.  5.  0.]
label :  3
predict :  (3, 1.0)
============================
dataset :  [ 0.  0.  7. 14. 11.  1.  0.  0.  0.  6. 15.  6.  7. 10.  0.  0.  0. 11.
  7.  0.  2. 12.  0.  0.  0.  5.  4.  0.  1. 12.  0.  0.  0.  0.  0.  0.
  3.  9.  0.  0.  0.  0.  0.  0. 11.  3.  0.  0.  0.  0.  0.  9. 13.  2.
  3.  0.  0.  0.  7. 16. 16. 16. 16.  6.]
label :  2
predict :  (2, 0.9999999999999847)
============================
dataset :  [ 0.  0.  4. 15. 15.  4.  0.  0.  0.  6. 16. 16. 12. 14.  0.  0.  0. 11.
 11.  6. 14. 12.  0.  0.  0.  3. 14. 13. 14.  1.  0.  0.  0.  0. 12. 16.
  5.  0.  0.  0.  0.  1. 16. 13.  9.  0.  0.  0.  0.  0. 13. 10. 15.  0.
  0.  0.  0.  0.  3. 15. 15.  0.  0.  0.]
label :  8
predict :  (8, 2.169826425980222e-13)
============================
dataset :  [ 0.  1. 12. 12. 12. 15.  6.  0.  0.  1. 14.  5.  5.  4.  1.  0.  0.  0.
 12.  0.  0.  0.  0.  0.  0.  8. 16. 16. 15.  8.  0.  0.  0.  1.  4.  4.
  5. 12.  7.  0.  0.  0.  0.  0.  0. 11.  4.  0.  0.  2.  7.  2. 10. 12.
  0.  0.  0.  2. 16. 15.  8.  1.  0.  0.]
label :  5
predict :  (5, 1.0)
============================
dataset :  [ 0.  0. 11. 10.  8. 12.  1.  0.  0.  0. 16. 13. 12. 10.  0.  0.  0.  5.
 14.  2.  0.  0.  0.  0.  0.  7. 16. 16. 13.  8.  0.  0.  0.  0.  4.  4.
  8. 16.  7.  0.  0.  0.  0.  0.  0.  9.  8.  0.  0.  0.  9.  4.  7. 16.
  5.  0.  0.  0. 14. 16. 14.  7.  0.  0.]
label :  5
predict :  (5, 1.0)
============================
dataset :  [ 0.  2.  6. 10. 12.  1.  0.  0.  0. 14. 13. 10.  5.  1.  0.  0.  0. 10.
  6.  0.  0.  0.  0.  0.  0. 10. 13. 12. 12.  5.  0.  0.  0.  2.  8.  5.
  7. 14.  8.  0.  0.  0.  0.  0.  0.  5. 12.  0.  0.  0.  2.  2.  1. 10.
 10.  0.  0.  0.  5. 16. 16. 14.  1.  0.]
label :  5
predict :  (5, 1.0)
============================
dataset :  [ 0.  0.  8.  9.  8. 12.  8.  0.  0.  0. 12. 14. 10.  8.  5.  0.  0.  1.
 14.  2.  0.  0.  0.  0.  0.  6. 16. 12. 12.  8.  0.  0.  0.  1.  4.  4.
  7. 15.  7.  0.  0.  0.  0.  0.  0. 10.  7.  0.  0.  1. 12.  4.  9. 15.
  1.  0.  0.  0.  9. 16. 14.  3.  0.  0.]
label :  5
predict :  (5, 1.0)
============================
dataset :  [ 0.  0.  8. 13. 12.  3.  0.  0.  0.  6. 15.  7.  9. 12.  0.  0.  0.  0.
  0.  0.  7. 11.  0.  0.  0.  0.  0.  4. 15.  3.  0.  0.  0.  0.  0. 10.
 16.  9.  0.  0.  0.  0.  0.  0.  4. 15.  7.  0.  0.  0. 11.  2.  1. 15.
  7.  0.  0.  0.  8. 16. 16. 12.  0.  0.]
label :  3
predict :  (3, 0.9999999999535079)
============================
dataset :  [ 0.  0. 11. 16. 10.  1.  0.  0.  0.  1. 15. 14. 15. 11.  0.  0.  0.  7.
 14.  1.  4. 16.  3.  0.  0.  7. 13.  0.  0. 10. 11.  0.  0.  9. 12.  0.
  0.  8. 12.  0.  0.  5. 14.  0.  0.  7. 13.  0.  0.  1. 16. 10.  5. 15.
  8.  0.  0.  0.  7. 16. 16. 15.  0.  0.]
label :  0
predict :  (0, 1.0)
============================
dataset :  [ 0.  0.  2. 12.  1.  0.  0.  0.  0.  0.  6. 13.  0.  0.  0.  0.  0.  0.
 13.  4.  0.  0.  0.  0.  0.  0. 16.  1.  0.  0.  0.  0.  0.  5. 16. 16.
 16. 10.  1.  0.  0.  2. 15.  6.  1. 10.  8.  0.  0.  0. 10. 10.  0.  9.
  9.  0.  0.  0.  2. 12. 16. 15.  1.  0.]
label :  6
predict :  (6, 1.0)
============================
dataset :  [ 0.  2. 15. 13.  1.  0.  0.  0.  0. 13. 12. 14.  7.  0.  0.  0.  0.  8.
  6.  6. 13.  0.  0.  0.  0.  0.  0.  5. 15.  0.  0.  0.  0.  0.  0. 11.
 10.  0.  0.  0.  0.  0.  2. 16.  5.  0.  0.  0.  0.  0. 11. 16. 13. 14.
 12.  0.  0.  3. 16. 14.  8.  8.  7.  0.]
label :  2
predict :  (2, 1.0)
============================

```

