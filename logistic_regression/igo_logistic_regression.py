import math
import numpy as np
import random
import scipy
from orderedset import OrderedSet

class LogisticRegression:


    def __init__(self, epoch,step_size,method="adam",verbose_training=False):
        self.X = []
        self.Y = []
        self.W = []
        self.epoch = epoch
        self.targets = []
        self.step_size = step_size
        self.verbose_training = verbose_training
        
        if method == "chord":
            self.optimizer = self.chord_optimizer
        elif method =="adam":
            self.optimizer = self.adam_optimizer
                                          
    def predict(self,x):
    
        maxtheta = -math.inf
        max_index = -1   
        for m in self.targets:
            eta = np.dot(x,self.W[m])
            theta = self.logistic_sigmoid(eta)
            if theta > maxtheta:
                maxtheta = theta
                max_index = m                
        return max_index,maxtheta
       
    def logistic_sigmoid(self,eta):
            return scipy.special.expit(eta)

            
    def fit(self,X,Y):  
        self.optimizer(X,Y)
        

    def chord_optimizer(self,X,Y):
        self.X = X
        self.Y = Y
        self.targets = OrderedSet(Y)
        self.W = [[random.randint(-3,3) for r in range(0,len(X[0]))] for t in range(0,len(self.targets)) ] 

        for n,x in enumerate(X):

            gradient = []
            Hessian = []
            iHessian = []

            for i in range(len(self.W[0])):
                Hessian.append([])
                for j in range(len(self.W[0])):
                    Hessian[i].append(x[i] * x[j])
            
            iHessian = np.linalg.pinv(np.array(Hessian))
 
            for m in self.targets:                
                for k in range(self.epoch) : 
                    gradient = []
                    eta = np.dot(np.array(x),np.array(self.W[m]))
                    theta = self.logistic_sigmoid(eta)
                    y=-1
                    
                    if Y[n] == m:
                        y=1
                    else:
                        y=0
                    
                    for i in range(len(self.W[m])):          
                        gradient.append(x[i]*(y - theta))
           
                    gradient = np.array(gradient)
                    training_unit = iHessian.dot(gradient)
                    error_min = math.inf
                    meetw = self.W[m]
                    selected_step = 0
                    
                    for step in range(1,self.step_size+1):
                        wtemp = np.add(self.W[m], step * training_unit)
                        error_temp = abs(y-self.logistic_sigmoid(np.dot(np.array(x),np.array(wtemp))))
                        
                        if error_min > error_temp:
                            meetw = wtemp
                            error_min = error_temp
                            selected_step = step
                    self.W[m] = meetw
                    
                    if self.verbose_training == True:
                        print("epoch(",k,") ",m," target error : ",error_min,"... step : ",selected_step)    
            
    def adam_optimizer(self,X,Y):
        self.X = X
        self.Y = Y
        self.targets = OrderedSet(Y)
        self.W = [[random.randint(-3,3) for r in range(0,len(X[0]))] for t in range(0,len(self.targets)) ] 
        alpha = 0.01
        beta_1 = 0.9
        beta_2 = 0.999
        epsilon = 1e-8


        for n,x in enumerate(X):
            gradient = []      
            for m in self.targets:
                m_t = 0
                v_t = 0
                for k in range(1,self.epoch+1):
                
                    gradient = []
                    eta = np.dot(np.array(x),np.array(self.W[m]))
                    theta = self.logistic_sigmoid(eta)
                    y=-1
                    
                    if Y[n] == m:
                        y=1
                    else:
                        y=0
                    
                    ths = []
                    for i in range(len(self.W[m])):          
                        gradient.append(-x[i]*(y - theta))
                        ths.append(x[i])
                    g_t = np.array(gradient)
                    m_t = beta_1*m_t + (1-beta_1)*g_t
                    v_t = beta_2*v_t + (1-beta_2)*np.multiply(g_t,g_t)
                    m_cap = m_t/(1-(beta_1**k))
                    v_cap = v_t/(1-(beta_2**k))
                    W_prev = self.W[m]
                    factor = 1
                    if abs(y - theta) == 1:
                        factor = 10
                    self.W[m] = self.W[m] - factor * (alpha*m_cap)/(np.sqrt(v_cap)+epsilon)

                    if self.verbose_training == True:
                        print("epoch("+str(k)+"),target:",m," error : ",abs(y - theta))    

                    if np.array_equal(self.W[m],W_prev) :
                        break
