# TODO list



## common

1. <del>Import Hash Table class</del>
2. <del>Import Graph class</del>
3. Bug fix : RB tree delete function
4. <del>Improve exception class</del>

    <del>\* apply \_\_func\_\_, \_\_FILE\_\_, \_\_LINE\_\_</del>
5. Optimize BigInteger mul/div operation
6. Apply index operator overloading
7. Build pruned Graph with reference to dikjstra
8. Linear Algebra Class

    \* addition
    
    \* multiplication

    \* inversing

    \* psuedo inversing

9. Refactoring

## decision tree

1. <del>Write python prototype</del>

2. Apply random state

3. Write RandomForest class

4. Wirte c++ library, inheriting class Tree

## CNN

1. convolutional layer

2. maxpooling layer

3. backpropagation


## RNN

1. recurrent hidden unit

2. LSTM

3. GRU
