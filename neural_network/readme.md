# 1. Usage
## 1.1 Python
### 1.1.1 Example
```
from igo_neural_network import NeuralNetwork
from sklearn.datasets import *
import sys
skdata = load_digits()

dataset = skdata["data"]
labels = skdata["target"]
test_data = [ 0,  0, 11, 10, 12,  4,  0,  0,  
0,  0, 12, 13,  9, 16,  1,  0,  
0,  0,  7, 13, 11, 16,  0,  0,  
0,  0,  1, 16, 15,  4,  0,  0,  
0,  0, 10, 16, 13,  0,  0,  0,  
0,  0, 14,  7, 12,  7,  0,  0,  
0,  4, 14,  4, 12, 13,  0,  0,  
0,  1, 11, 14, 12,  4,  0,  0]


nl = NeuralNetwork(epoch=20,learning_rate=0.001,optimizer="adam",process_verbose=False,training_verbose=False)
#nl = NeuralNetwork(epoch=20,learning_rate=0.001,optimizer="gradient_descent",process_verbose=True,training_verbose=True)

#nl.build_model(64,[80],10)
#nl.build_model(64,[],10)
nl.build_model(input_layer_units=64,hidden_layers=[40,30,25,20],output_layer_units=10)


nl.brief_model()


nl.fit(skdata["data"],skdata["target"])
print(nl.predict(test_data))
````

### 1.1.2 Constructor
요약 : 하이퍼 파라미터 정의

epoch : 전체 데이터셋에 대한 반복 학습 횟수

learning_rate : 0.001 권장

optimizer : adam method와 gradient_descent 중에 택 1

process_verbose : 학습 진행 과정 시각화

training_verbose : 학습률 시각화


### 1.1.3 Model Builder
요약 : 모델 구조 정의

input_layer_units : 입력 벡터 차원

hidden_layers : 유닛들 갯수로 이루어진 list

output_layer_units : 출력 계층 유닛 수


# 2. 인공신경망 개요

## 2.1 Model

입력계층 : 입력데이터=5-dimensional numerical vector

은닉계층 : 계층 수 = 4, 계층별 뉴런 = 4,4,3,2

출력계층 : activation함수=logistic sigmoid, 출력 유닛 수 = 2



### 2.1.1 Figure

### ![](C:\Users\user\Desktop\AI자료\neural_network\model.png)

### 2.1.2 Notation

$w_{layer\_index,neuron\_index}$

 : $w_{0,0}=(w_{0,0,0},w_{0,0,1},w_{0,0,2},w_{0,0,3})$, $w_{3,1}=(w_{3,1,0},w_{3,1,1})$

$\eta_{layer\_index,neuron\_index}$ : $\eta_{0,0}=x\cdot w_{0,0}$, $\eta_{3,1}=\theta_2\cdot w_{3,1}$

$\theta_{layer\_index,neuron\_index}:$ $\theta_{0,0}=x_1$,  $\theta_{4,0}=tanh(\eta_{4,0})$, $\theta_{3,2}=tanh(\eta_{3,2})$

## 2.2 Forward propagation

$\theta_{-1,0}=x_0$

$\theta_{-1,1}=x_1$

$\theta_{-1,2}=x_2$

$\theta_{-1,3}=x_3$

$\theta_{-1,4}=x_4$



$\theta_{0,0}=h(w_{0,0}\cdot\theta_{-1})=h(\displaystyle\sum_{i=0}^{4}{w_{0,0,i}*\theta_{-1,i}})$

$\theta_{0,1}=h(w_{0,1}\cdot\theta_{-1})=h(\displaystyle\sum_{i=0}^{4}{w_{0,1,i}*\theta_{-1,i}})$

$\theta_{0,2}=h(w_{0,2}\cdot\theta_{-1})=h(\displaystyle\sum_{i=0}^{4}{w_{0,2,i}*\theta_{-1,i}})$

$\theta_{0,3}=h(w_{0,3}\cdot\theta_{-1})=h(\displaystyle\sum_{i=0}^{4}{w_{0,3,i}*\theta_{-1,i}})$





$\theta_{1,0}=h(w_{1,0}\cdot\theta_0)=h(\displaystyle\sum_{j=0}^{3}{w_{1,0,j}*h(\displaystyle\sum_{i=0}^{4}{w_{0,j,i}*\theta_{-1,i}})})$

$\theta_{1,1}=h(w_{1,1}\cdot\theta_0)=h(\displaystyle\sum_{j=0}^{3}{w_{1,1,j}*h(\displaystyle\sum_{i=0}^{4}{w_{0,j,i}*\theta_{-1,i}})})$

$\theta_{1,2}=h(w_{1,2}\cdot\theta_0)=h(\displaystyle\sum_{j=0}^{3}{w_{1,2,j}*h(\displaystyle\sum_{i=0}^{4}{w_{0,j,i}*\theta_{-1,i}})})$

$\theta_{1,3}=h(w_{1,3}\cdot\theta_0)=h(\displaystyle\sum_{j=0}^{3}{w_{1,3,j}*h(\displaystyle\sum_{i=0}^{4}{w_{0,j,i}*\theta_{-1,i}})})$





$\theta_{2,0}=h(w_{2,0}\cdot\theta_1)=h(\displaystyle\sum_{k=0}^{2}{w_{2,0,k}*h(\displaystyle\sum_{j=0}^{3}{w_{1,k,j}*h(\displaystyle\sum_{i=0}^{4}{w_{0,j,i}*\theta_{-1,i}})})})$

$\theta_{2,1}=h(w_{2,1}\cdot\theta_1)=h(\displaystyle\sum_{k=0}^{2}{w_{2,1,k}*h(\displaystyle\sum_{j=0}^{3}{w_{1,k,j}*h(\displaystyle\sum_{i=0}^{4}{w_{0,j,i}*\theta_{-1,i}})})})$

$\theta_{2,2}=h(w_{2,2}\cdot\theta_1)=h(\displaystyle\sum_{k=0}^{2}{w_{2,2,k}*h(\displaystyle\sum_{j=0}^{3}{w_{1,k,j}*h(\displaystyle\sum_{i=0}^{4}{w_{0,j,i}*\theta_{-1,i}})})})$





$\theta_{3,0}=h(w_{3,0}\cdot\theta_2)=h(\displaystyle\sum_{m=0}^{1}{w_{3,0,m}*h(\displaystyle\sum_{k=0}^{2}{w_{2,m,k}*h(\displaystyle\sum_{j=0}^{3}{w_{1,k,j}*h(\displaystyle\sum_{i=0}^{4}{w_{0,j,i}*\theta_{-1,i}})})})})$

$\theta_{3,1}=h(w_{3,1}\cdot\theta_2)=h(\displaystyle\sum_{m=0}^{1}{w_{3,1,m}*h(\displaystyle\sum_{k=0}^{2}{w_{2,m,k}*h(\displaystyle\sum_{j=0}^{3}{w_{1,k,j}*h(\displaystyle\sum_{i=0}^{4}{w_{0,j,i}*\theta_{-1,i}})})})})$



$t_{0}=\theta_{4,0}=sigmoid( w_{4,0}\cdot\theta_3)=sigmoid(\displaystyle\sum_{n=0}^{1}{(w_{4,0,n}*h(\displaystyle\sum_{m=0}^{1}{w_{3,n,m}*h(\displaystyle\sum_{k=0}^{2}{w_{2,m,k}*h(\displaystyle\sum_{j=0}^{3}{w_{1,k,j}*h(\displaystyle\sum_{i=0}^{4}{w_{0,j,i}*\theta_{-1,i}})})})}))})$

$t_{1}=\theta_{4,1}=sigmoid( w_{4,1}\cdot\theta_3)=sigmoid(\displaystyle\sum_{n=0}^{1}{(w_{4,1,n}*h(\displaystyle\sum_{m=0}^{1}{w_{3,n,m}*h(\displaystyle\sum_{k=0}^{2}{w_{2,m,k}*h(\displaystyle\sum_{j=0}^{3}{w_{1,k,j}*h(\displaystyle\sum_{i=0}^{4}{w_{0,j,i}*\theta_{-1,i}})})})}))})$







## 2.3 Back propagation

$w_{i,j}^{(k)}=w_{i,j}^{(k)}+\mathcal J_{i,j}^{-1}(y=m)\cdot U_{i,j}(y=m)$ , where($m\in\{0,1\}$,$i\in\{0,1,2,3\}$ ,$j\in\{0,1,2,3,4\}$)



$U_{i,j}(y=m)$ : gradient.pdf 참조

$\cal J_{i,j}(y=m)$ : information.md 참조





# 3. Algorithm

```

	def __init__(self, epoch, step_size):

		self.epoch = epoch
		self.step_size = step_size
		self.targets = []
		self.W = []
		self.theta = []
		self.input_layer_units = 0
		self.hidden_layers = []
		self.output_layer_units = 0


	def model_build(self, input_layer_units, hidden_layers, output_layer_units):
	
		self.input_layer_units = input_layer_units 
		self.hidden_layers = hidden_layers
		self.output_layer_units = output_layer_units
		self.total_layers = 2+len(hidden_layers)
	

		for i,n in enumerate(hidden_layers):
			self.theta.append([j for j in range(0,n)])

		self.theta.append([j for j in range(0,output_layer_units) ])
		self.theta.append([j for j in range(0,input_layer_units) ])
		
		for i in range(len(hidden_layers)):
			self.W.append([])
			for j in range(hidden_layers[i]):
				self.W[i].append([random.randint(-10,10) for r in range(len(self.theta[i-1]))])

		self.W.append([[random.randint(-10,10) for s in self.theta[-2]] for r in range(output_layer_units)])


	def logistic_sigmoid(self,eta):
		return scipy.special.expit(eta)


	def forward_propagate(self,x):

		for i in range(0, len(self.theta)-2 ):
			for j in range(0, len(self.theta[i])):
				eta = np.dot(self.theta[i-1],self.W[i][j])
				self.theta[i][j] = np.tanh(eta)

		for j in range(0,len(self.theta[-2])):
			eta = np.dot(self.theta[-3],self.W[-1][j])
			self.theta[-2][j] = self.logistic_sigmoid(eta)
			
	def predict(self,x):
	
		maxtheta = -math.inf
		max_index = -1
		
		self.forward_propagate(x)
		
		for m in range(self.output_layer):
			if self.theta[-2][m] > maxtheta:
				maxtheta = self.theta[-2][m]
				max_index = m
				
				
		return max_index,maxtheta	
			
			
	def fit(self,X,Y):
		self.X = X
		self.Y = Y
		self.targets = OrderedSet(Y)
		
		for i,x in enumerate(X):
			self.forward_propagate(x)
			self.back_propagate(Y[i])	
	
	def back_propagate(self,target):

		for output_unit_index in range(len(self.W[-1])):
			y=-1
			if target == output_unit_index:
				y=1
			else:
				y=0

			gradient = []


			for layer_index in range(len(self.W)-1,-1,-1):
				for unit_index in range(len(self.W[layer_index])):
					
					result_vectors = self.calc_partial(self.total_layers-1, layer_index,unit_index)
					gradient = (y-self.theta[-1][output_unit_index]) * result_vectors
					Hessian = []
					iHessian = []
					for i,result_vector1 in enumerate(result_vectors):
						Hessian.append([])
						for result_vector2 in result_vectors:
							Hessian[i].append(result_vector1 * result_vector2)
					iHessian = np.linalg.pinv(np.array(Hessian))
					self.W[layer_index][unit_index] = self.W[layer_index][unit_index] + np.dot(iHessian,gradient)
			
			
			
	def calc_partial(self,current_layer_index,aim_layer_index,aim_unit_index):
		print("current_layer : ",current_layer_index,"aimlayer : ",aim_layer_index,"aimunit : ",aim_unit_index)
		output_layer_index = len(self.W)-1
		
		if output_layer_index == aim_layer_index :
			return [theta for theta in self.theta[-2]]

		if current_layer_index == aim_layer_index :
			res = []
			for i,w in enumerate(self.W[aim_layer_index][aim_unit_index]):
				res.append(w*( mpmath.sech(np.dot(w,self.theta[current_layer_index-1])) ** 2)*self.theta[current_layer_index - 1][i])
			return res

		results = self.calc_partial(current_layer_index-1,aim_layer_index,aim_unit_index)
		

		for result in results :
			temp = 0
			for i,w in enumerate(self.W[current_layer_index][aim_unit_index]):
				temp += w * ( mpmath.sech(np.dot(w,self.theta[current_layer_index-1])) ** 2) * result
			result = temp 
			
			
```