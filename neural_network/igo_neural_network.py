import math
import numpy as np
import random
import scipy
from orderedset import OrderedSet
import sys
from copy import deepcopy
class NeuralNetwork:


    def __init__(self, epoch, learning_rate,optimizer="adam",process_verbose=False,training_verbose=False):
        self.epoch = epoch
        self.learning_rate = learning_rate
        self.targets = []
        self.W = []
        self.theta = []
        self.input_layer_units = 0
        self.hidden_layers = []
        self.output_layer_units = 0
        self.output_layer_index = -1
        self.process_verbose = process_verbose
        self.training_verbose = training_verbose
        if optimizer == "gradient_descent" : 
            self.optimizer = self.gradient_descent_optimizer
        elif optimizer == "adam":
            self.optimizer = self.adam_optimizer
              
        sys.setrecursionlimit(10000)
        

        
        
    def brief_model(self):
        print("input_layer_units : ", self.input_layer_units)
        print("hidden_layers : ",self.hidden_layers)
        print("output_layer_units : ",self.output_layer_units)
        print("total_layers : ",self.total_layers)
        print("output_layer_index : ", self.output_layer_index)
        print("theta parameters : ")
        for i,theta in enumerate(self.theta):
            print("\tindex(",i,")",len(theta))
        print("weights : ")
        for i,weight in enumerate(self.W):
            print("\tindex(",i,")",len(weight))
        
        
    def build_model(self, input_layer_units, hidden_layers, output_layer_units):
    
        self.input_layer_units = input_layer_units 
        self.hidden_layers = hidden_layers
        self.output_layer_units = output_layer_units
        self.total_layers = 2+len(hidden_layers)
        self.output_layer_index = len(hidden_layers)
    
        for i,n in enumerate(hidden_layers):
            self.theta.append([j for j in range(0,n)])
        self.theta.append([j for j in range(0,output_layer_units) ])
        self.theta.append([j for j in range(0,input_layer_units) ])
        
        for i in range(len(hidden_layers)):
            self.W.append([])
            for j in range(hidden_layers[i]):
                self.W[i].append([random.randint(-50,50)/10.0 for r in range(len(self.theta[i-1]))])
        if len(self.theta) != 2:            
            self.W.append([[random.randint(-50,50)/10.0 for s in self.theta[-3]] for r in range(output_layer_units)])
        else : 
            self.W.append([[random.randint(-50,50)/10.0 for s in self.theta[-1]] for r in range(output_layer_units)])
    
    def logistic_sigmoid(self,eta):
        return scipy.special.expit(eta)
    def relu(self,eta):
        if eta > 0:
            return eta
        else : 
            return 0
    def drelu(self,eta):
        if eta > 0:
            return 1
        else : 
            return 0
        
    def forward_propagate(self,x):
        self.theta[-1] = x
        if len(self.theta) == 2 :
            
            for j in range(0,len(self.theta[0])):

                eta = np.dot(self.theta[-1],self.W[0][j])
                self.theta[0][j] = self.logistic_sigmoid(eta)
        else:
            for i in range(0, len(self.theta)-2 ):
                for j in range(0, len(self.theta[i])):
                    eta = np.dot(self.theta[i-1],self.W[i][j])
                    self.theta[i][j] = self.relu(eta)
                    
            for j in range(0,len(self.theta[-2])):
                eta = np.dot(self.theta[-3],self.W[-1][j])
                self.theta[-2][j] = self.logistic_sigmoid(eta)
    def predict(self,x):
    
        maxtheta = -math.inf
        max_index = -1
        
        self.forward_propagate(x)
        
        for m in range(self.output_layer_units):
            if self.theta[-2][m] > maxtheta:
                maxtheta = self.theta[-2][m]
                max_index = m
                
                
        return max_index,maxtheta    
            
            
    def fit(self,X,Y):
        self.X = X
        self.Y = Y
        
        for e in range(self.epoch):
            for i,x in enumerate(X):
                if self.process_verbose == True:
                    print("fitting for ",i,"th data point. label is",self.Y[i],", in epoch",e+1)
                self.back_propagate(x,Y[i],i)    
            
            
    def back_propagate(self,x,target,i):
        self.optimizer(x,target,i)
    
    def gradient_descent_optimizer(self,x,target,i):
        for output_unit_index in range(len(self.theta[-2])):
            y=-1
            if target == output_unit_index:
                y=1
            else:
                y=0
                
            for tt in range(5):
                self.forward_propagate(x)
                
                if self.training_verbose == True: 
                    print("\trepeatition("+str(tt)+"),target:",output_unit_index," theta :",self.theta[-2][output_unit_index],"... y :",y," error : ",abs(y-self.theta[-2][output_unit_index]))
                if np.float64(1.0e-8) > abs(y-self.theta[-2][output_unit_index]):
                    break

                wtemp = deepcopy(self.W)
                
                for layer_index in range(len(self.theta)-2,-1,-1):
                    if layer_index == self.output_layer_index:
                        gradient = []
                        Hessian = []
                        iHessian = []
                        factor = 1
                        
                        result_vectors = self.calc_partial(target,self.output_layer_index, layer_index,output_unit_index)
                        gradient = (y-self.theta[-2][output_unit_index]) * np.array(result_vectors)
                        for i,result_vector1 in enumerate(result_vectors):
                            Hessian.append([])
                            for result_vector2 in result_vectors:
                                Hessian[i].append(result_vector1*result_vector2)
                        iHessian = np.linalg.pinv(np.array(Hessian))
                        if abs(y - self.theta[-2][output_unit_index]) == 1:
                            factor = 10
                        wtemp[layer_index][output_unit_index] = self.W[layer_index][output_unit_index] + factor*np.dot(iHessian,gradient)
                        continue
                        
                    for unit_index in range(len(self.W[layer_index])):
                        gradient = []
                        Hessian = []
                        iHessian = []
                        
                        result_vectors = self.calc_partial(target,self.output_layer_index, layer_index,unit_index)
                        gradient = (y-self.theta[-2][output_unit_index]) * np.array(result_vectors)
                        for i,result_vector1 in enumerate(result_vectors):
                            Hessian.append([])
                            for result_vector2 in result_vectors:
                                Hessian[i].append(result_vector1*result_vector2)
                        iHessian = np.linalg.pinv(np.array(Hessian))
                        wtemp[layer_index][unit_index] = self.W[layer_index][unit_index] + np.dot(iHessian,gradient)
                
                self.W = deepcopy(wtemp)
            
    def adam_optimizer(self,x,target,i):
        
        alpha = self.learning_rate
        beta_1 = 0.9
        beta_2 = 0.999
        epsilon = 1e-8
        for output_unit_index in range(len(self.theta[-2])):
            y=-1
            if target == output_unit_index:
                y=1
            else:
                y=0
                
            for tt in range(5):
                self.forward_propagate(x)
                if self.training_verbose == True: 
                    print("\tepoch("+str(tt)+"),target:",output_unit_index," theta :",self.theta[-2][output_unit_index],"... y :",y," error : ",abs(y-self.theta[-2][output_unit_index]))
                if np.float64(1.0e-8) > abs(y-self.theta[-2][output_unit_index]):
                    break
                    
                wtemp = deepcopy(self.W)
                for layer_index in range(self.output_layer_index,-1,-1):
                    if layer_index == self.output_layer_index:
                        m_t = 0
                        v_t = 0
                        k=1
                        gradient = []
                        
                        result_vectors = self.calc_partial(target,self.output_layer_index, layer_index,output_unit_index)
                        gradient = -(y-self.theta[-2][output_unit_index]) * np.array(result_vectors)
                        g_t = gradient
                        m_t = beta_1*m_t + (1-beta_1)*g_t
                        v_t = beta_2*v_t + (1-beta_2)*np.multiply(g_t,g_t)
                        m_cap = m_t/(1-(beta_1**k))
                        v_cap = v_t/(1-(beta_2**k))
                        problem = (alpha*m_cap)/(np.sqrt(v_cap)+epsilon)
                        if abs(y - self.theta[-2][output_unit_index]) == 1:
                            problem *= 10/self.learning_rate
                        wtemp[layer_index][output_unit_index] = self.W[layer_index][output_unit_index] - problem
                        
                        continue

                    for unit_index in range(len(self.W[layer_index])):
                        m_t = 0
                        v_t = 0
                        k=1
                        gradient = []
                        
                        result_vectors = self.calc_partial(target,self.output_layer_index, layer_index,unit_index)
                        gradient = -(y-self.theta[-2][output_unit_index]) * np.array(result_vectors)
                        g_t = gradient
                        m_t = beta_1*m_t + (1-beta_1)*g_t
                        v_t = beta_2*v_t + (1-beta_2)*np.multiply(g_t,g_t)
                        m_cap = m_t/(1-(beta_1**k))
                        v_cap = v_t/(1-(beta_2**k))
                        W_prev = self.W[layer_index][unit_index]
                        problem = (alpha*m_cap)/(np.sqrt(v_cap)+epsilon)              
                        wtemp[layer_index][unit_index] = self.W[layer_index][unit_index] - problem
        
                self.W = deepcopy(wtemp)  

               
                

    def calc_partial(self,target,current_layer_index,aim_layer_index,aim_unit_index):

        if self.output_layer_index == aim_layer_index :
            if len(self.theta) != 2 :
                return [theta for theta in self.theta[-3]]
            else :
                return [theta for theta in self.theta[-1]]
            
        if self.output_layer_index - 1 == aim_layer_index :
            res = []
            for i,w in enumerate(self.W[aim_layer_index][aim_unit_index]):
                eta = np.dot(self.W[aim_layer_index][aim_unit_index],self.theta[aim_layer_index-1])
                temp = self.W[current_layer_index][target][aim_unit_index]*self.drelu(eta)*self.theta[aim_layer_index - 1][i]
                res.append(temp)
            return np.array(res,dtype=float)
            
            
        if current_layer_index == self.output_layer_index:
            current_layer_index -= 1



        if current_layer_index-1 == aim_layer_index :
            res = []
            temp = 0

            for i in range(len(self.W[current_layer_index])):
                part1 = self.W[current_layer_index+1][target][i]*self.drelu(np.dot(self.W[current_layer_index][i],self.theta[current_layer_index-1]))
                part2 = self.W[current_layer_index][i][aim_unit_index]*self.drelu(np.dot(self.W[aim_layer_index][aim_unit_index],self.theta[aim_layer_index-1]))	
                temp += part1 * part2

            for i in range(len(self.W[aim_layer_index][aim_unit_index])):
                res.append(temp * self.theta[aim_layer_index - 1][i])

            return np.array(res,dtype=float)
	
		
        res = []
        for local_target,w in enumerate(self.W[current_layer_index]):
            result_vectors = self.calc_partial(local_target,current_layer_index-1,aim_layer_index,aim_unit_index)
            result_vectors = result_vectors * self.W[current_layer_index+1][target][local_target] * self.drelu(np.dot(self.W[current_layer_index][local_target],self.theta[current_layer_index-1]))

        return np.array(result_vectors,dtype=float)
