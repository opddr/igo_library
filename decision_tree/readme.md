# Usage

## python

```
from igo_decision_tree import DecisionTree

dectree = DecisionTree()
dectree.fit_with_jsonfile( "filename_jsons.txt",depth )
print(dectree.predict( inputdata_jsonformat ))
dectree.render(  )
```

#### filename_jsons.txt

* 예시
```
{'@description': 'https://littlebigfarms.co.za/wp-content/uploads/2020/04/navy/navyfederal.org-login.verify-authenticate/my/accounts.navyfederal.orgNFCU/', '@label': 1, 'title': -1, 'search_domain': -1, 'registered_date': -1, 'ct_size': 4, 'issuer_confidence': 0}
{'@description': 'https://epost.go.kr', '@label': 0, 'title': -1, 'search_domain': 10, 'registered_date': -1, 'ct_size': 32, 'issuer_confidence': 0}
{'@description': 'https://www.naver.com', '@label': 0, 'title': 10, 'search_domain': 10, 'registered_date': -1, 'ct_size': 0, 'issuer_confidence': 0}
```

##### Attribute
key : @로 시작하지 않는 임의의 문자열

data : 특성의 정도를 묘사하는 숫자 데이터(정수, 실수). 임의의 실수 범위에서 양극에 위치한 attribute들은 성질이 상이해야 하며 하며, 중간값은 중간 특성을 묘사
 * <b>Note</b> : 입력된 json의 key들을 결정트리의 feature_names으로 인식. 따라서 모든 데이터들의 key의 종류와 갯수는 동일 해야 한다.
 

##### Directive
'@label' : data의 label. 0이상의 자연수

'<span>&#64;</span>description' : 데이터 처리에서 무시되는 directive로, comment로 활용


#### inputdata_jsonformat
* <b>예시</b>
```
{'title': 10, 'search_domain': 10, 'registered_date': -1, 'ct_size': 0, 'issuer_confidence': 0}
```


* <b>Note</b> : inputdata의 key들은 input file의 directive들을 제외한 key들의 종류와 갯수가 동일해야 한다.


# 결정트리 개요

결정 트리는 데이터 셋들을 분석하여 다수의 decision rule을 만들어 대상을 분류하는 머신러닝 알고리즘 이다. 

데이터 집합을 분석하여 생성한 decision rule들은 트리형태로 관리되며, 트리의 각 노드들은 feature와 threshold를 합친 개념인 attribute로 이루어진다.

결정트리는 정보력이 가장 강한 attribute가 root되고, 정보력이 낮은 atttribute들이 child를 이룬다. 

결정 트리에서는 데이터 집합을 특정 attribute를 기준으로 나누었을 때, 나뉘어진 두 집합의 impurity가 낮게 되도록 하는 attribute를 정보력이 강한 attribute로 간주한다.

impurity는 Gini와 Information Entropy로 측정하며 수식은 아래와 같다.<div align="center">
$`Gini(X)=\displaystyle\sum_k{p_{k}(1-p_{k})}`$

$`Entropy(X)=-\displaystyle\sum_k{p_{k}\log p_{k}}`$
</div>

위 수식에서 $`X`$ 은 training dataset이며 $`p_k`$는 dataset에서 label이 k인 데이터들의 비율이다.<div align="center">
$`p_k=\frac{The\space number\space of\space datas\space with\space label k}{The\space number\space of\space\space datas\space in entire\space dataset}`$
</div>

결론적으로 결정트리는 root에서 시작하여 재귀적으로 node들을 만들어 가는데, 각 스테이지에서 데이터 집합의 impurity를 가장 낮추는 attribute를 노드로 선정한다.

# scikit-learn implementation

$`m`$: 스테이지

$`Q_m`$ : m 스테이지에서의 데이터셋. 

$`N_m`$: $`Q_m`$의 크기. $`Q_1`$은 루트의 데이터셋

$`j`$ : Feature

$`t`$ : Threashold

$`\theta`$ : (j,t). 즉, feature와 threshold 쌍

$`Q_{left}(\theta)`$ : $`\theta`$로 데이터셋Q를 나누었을 때 왼쪽 집합

$`n_{left}`$: $`Q_{left}(\theta)`$ 의 크기

$`Q_{right}(\theta):`$ : $`\theta`$로 데이터셋Q를 나누었을 때 오른쪽 집합

$`n_{right}`$:$`Q_{right}(\theta)`$ 의 크기

$`H()`$ : 데이터 집합에 대한 impurity. Gini나 Entropy를 이용하여 계산

$`G(Q_m,\theta)`$:$`\frac{n_{left}}{N_m}H(Q_{left}(\theta))+\frac{n_{right}}{N_m}H(Q_{right}(\theta))`$를 계산하여 $`\theta^*=argmin_\theta G(Q,\theta)`$ 를 찾아서 현재 노드의 attribute로 설정

allowd depth에 이르거나 $`N_m<min_{samples},1`$가 되기 전까지 데이터 집합을 나누면서 재귀적으로 트리 구성.



# algorithm

```
divide(Dataset, Criteria[feature,threshold]) : 입력받은 데이터셋을 입력받은 criteria를 기준으로 양분하여 리턴 
def partitioning(array Dataset, array Feature) : 
	feature = 0, threshold = 0 # $`\theta^*=argmin_\theta G(Q,\theta)`$
	temp = 0 
	impurity = 0 # G(Q, θ)
	
	if( Dataset.len <= minlen || Dataset.len <= 1 || tree.depth <= depth_limit_specified_by_user )
		set current node as leaf with major class in current dataset
		
	for d in Dataset : 
		for f in Feature : 
			criteria = [ f , d[f] ];
			left, right = divide(Dataset, criteria)
			temp = (left.len / Dataset.len) * H(left)  +  (right.len / Dataset.len) * H(right)
			if( impurity > temp )
				impurity = temp
				feature = f , threshold = d[f];

	partitioning(left , feature)
	partitioning(right, feature)
```